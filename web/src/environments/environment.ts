// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  isLocal: true,
  jsonLink: 'https://plt1.coinshop.biz/api/v3/',
  javaLink: 'https://pltj1.coinshop.biz/ticker/api/',
  checkoutKey: 'pk_test_1bd138e6-9f81-4f9a-b2a7-25762405bacf',
  supportEmail: 'support@coinshop.biz',
  outgoingLinks: {
    homePage: 'https://dev1.coinshop.biz/',
    blog: 'https://blog.coinshop.biz/',
    linkTerms: 'https://dev1.coinshop.biz/legal-terms/',
    linkPrivacy: 'https://dev1.coinshop.biz/legal-terms/?subp=2',
    checkoutUrl: 'https://sandbox.checkout.com/js/v1/checkoutkit.js',
    cardPayUrl: 'https://sandbox.cardpay.com/MI/cardpayment.html'
  }
};
