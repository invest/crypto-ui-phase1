export const environment = {
  production: true,
  isLocal: false,
  jsonLink: 'https://##php_domain##/api/v3/',
  javaLink: 'https://##java_domain##/ticker/api/',
  checkoutKey: '##Checkout-belize-public_key##',
  supportEmail: 'support@coinshop.biz',
  outgoingLinks: {
    homePage: 'https://##wordpress_domain##/',
    blog: 'https://blog.coinshop.biz/',
    linkTerms: 'https://##wordpress_domain##/legal-terms/',
    linkPrivacy: 'https://##wordpress_domain##/legal-terms/?subp=2',
    checkoutUrl: 'https://cdn.checkout.com/js/v1/checkoutkit.js',
    cardPayUrl: 'https://cardpay.com/MI/cardpayment.html'
  }
};
