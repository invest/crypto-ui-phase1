import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from 'localize-router';

import { AppSettings } from './shared/app-settings';
import { UtilsService } from './shared/services/utils.service';
import { DocumentsService } from './user/services/documents.service';
import { UserService } from './user/services/user.service';
import { DepositSuccessCcComponent } from './platform/components/deposit-success-cc/deposit-success-cc.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public showHeaderFooter: Boolean = true;

  constructor(
    private appSettings: AppSettings,
    private user: UserService,
    private translate: TranslateService,
    private router: Router,
    private localize: LocalizeRouterService,
    private documents: DocumentsService,
    private utils: UtilsService,
    public dialog: MatDialog,
  ) {
    translate.setDefaultLang(appSettings.locale);
    translate.use(appSettings.locale);
  }

  ngOnInit() {
    if (this.appSettings.isLogged) {
      this.user.initUser();
      const ccResponse = this.utils.getUrlValue('ccResponse');

      if (ccResponse) {
        const translatedPathIn = this.localize.translateRoute('/platform/buy-crypto');
        this.router.navigate([translatedPathIn]);

        if (ccResponse) {
          const dialogRef = this.dialog.open(DepositSuccessCcComponent, {
            data: {status: ccResponse}
          });

          dialogRef.afterClosed().subscribe(result => {
            const translatedPath: any = this.localize.translateRoute('/platform/orders');
            this.router.navigate([translatedPath]);
          });
        }
      } else {
        this.documents.goToNextStep(true, true);
      }
    }
  }
}
