import { Location } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { MatDialogModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToasterModule } from 'angular2-toaster';
import { LocalizeParser, LocalizeRouterModule, LocalizeRouterSettings, ManualParserLoader } from 'localize-router';
import { LocalizeRouterHttpLoader } from 'localize-router-http-loader';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';

import { AppComponent } from './app.component';
import { PlatformModule } from './platform/platform.module';
import { AppErrorHandler } from './shared/app-error-handler';
import { AppSettings } from './shared/app-settings';
import { GuardLogin } from './shared/services/guard-login.service';
import { GuardLogout } from './shared/services/guard-logout.service';
import { StartupService } from './shared/services/startup.service';
import { SharedModule } from './shared/shared.module';
import { StaticModule } from './static/static.module';
import { UserModule } from './user/user.module';


export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, 'assets/i18n/', '.json');
}
export function HttpLoaderFactory_url(translate: TranslateService, location: Location, settings: LocalizeRouterSettings, http: HttpClient) {
  return new LocalizeRouterHttpLoader(translate, location, settings, http);
}
export function startupServiceFactory(startupService: StartupService) {
  return () => startupService.load();
}

const routes = [
  { path: '**', redirectTo: 'en/page-not-found' },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,

    StaticModule,
    UserModule,
    SharedModule,
    PlatformModule,

    RouterModule.forRoot(routes),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: (translate, location, settings, appSettings) =>
          new ManualParserLoader(translate, location, settings, appSettings.locales, 'YOUR_PREFIX'),
        deps: [TranslateService, Location, LocalizeRouterSettings, AppSettings]
      }
    }),
    RecaptchaModule.forRoot(),
    RecaptchaFormsModule,
    ToasterModule,
    MatDialogModule
  ],
  providers: [
    StartupService,
    GuardLogin,
    GuardLogout,
    // get some init requests first
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true
    }, {
      provide: ErrorHandler,
      useClass: AppErrorHandler
    },
    AppSettings
  ],
  entryComponents: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
