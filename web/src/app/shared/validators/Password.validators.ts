import { AbstractControl } from '@angular/forms';

import { UtilsService } from '../services/utils.service';

export class PasswordValidator {
  static matchInputs(control: AbstractControl) {
    if (control.root.get('password') && control.value !== control.root.get('password').value) {
      return { inputNotMatch: true };
    } else {
      return null;
    }
  }

  static passwordStrength(utils: UtilsService) {
    return (control: AbstractControl) => {
      if (!utils.calcPasswordStrength(control.value).goodEnough) {
        return { passwordStrength: true };
      } else {
        return null;
      }
    };
  }
}
