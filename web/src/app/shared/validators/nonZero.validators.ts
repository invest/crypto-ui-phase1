import { AbstractControl } from '@angular/forms';

export class SimpleValidators {
  static cannotBeNegative(control: AbstractControl) {
    if (Number(control.value) <= 0) {
      return { nonZero: true };
    } else {
      return null;
    }
  }
}
