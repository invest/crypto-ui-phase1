import { AbstractControl } from '@angular/forms';

import { CryptoService } from '../services/crypto.service';

export class BlockchainValidator {
  static validAddress(crypto: CryptoService) {
    return (control: AbstractControl) => {
      if (control.root.get('market')) {
        let valid = false;
        const coin = control.root.get('market').value.split('-')[1];

        if (coin.toLowerCase() === 'eth') {
          valid = crypto.isAddress(control.value);
        } else {
          valid = (<any>window).WAValidator.validate(control.value, coin);
        }

        if (!valid) {
          return { invalidWallet: {
            coin: coin
          } };
        } else {
          return null;
        }
      }
    };
  }
}
