
export class AppSettings {

  public _isLogged = false;

  public locale = 'en';

  public locales = ['en', 'tr', 'nl', 'it', 'fr', 'es', 'de','ar'];

  public settings = {
    intercomApiKey: '',
    recaptchaKey: '6LfH-lYUAAAAAFM2goSO9yF-L1KVSQKAGm8J_1kA',
    serverTime: 0,
    serverOffset: 0,
    // tslint:disable-next-line:max-line-length
    authorization: 'Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==',
    // tslint:disable-next-line:max-line-length
    authorizationUpload: 'Basic OUYxQ0Y3RTQyNUM1NkJGQUQ1QzQxOEMzMEQyN0I1M0I6QzFGNDU2NDgwODM2MzYyNDE4NzA1NzMxMzVBQjM4RTc3REM4RTNEOTU1REI2NUJGOURFQjA2RDVEMzY0RUFCQg=='
  };

  private _skinMap = {
    'en': {
      locale: 'en',
      isAllowed: true,
      // tslint:disable-next-line:quotemark
      regEx_letters: "^['a-zA-Z ]+$",
      name: 'English'
    }
  };

  private _responseCodeMap = {
    success: 0,
    network_error: 10000,
    server_error: 10001,
    user_exists: 1001,
    no_user: 1002,
    not_logged: 401,
    not_valid_prediction: 1004,
    low_balance: 1005,
    has_prediction: 1006,
    recaptcha_invalid: 1007,
    nickname_exists: 1008,
    token_exists: 1009,
    unknown_user: 1010,
    invalid_token: 1011,
    wallet_address_exists: 1012,
    wallet_api_problem: 1013,
    wallet_no_funds: 1014,
    not_in_whitelist: 8999
  };

  private _predictionStates = {
    created: 1,
    ready_for_review: 2,
    reviewed: 3,
    opened_for_predictions: 4,
    closed_for_predictions: 5,
    waiting_for_outcome: 6,
    outcome_provided: 7,
    dispute_opened: 8,
    outcome_approved: 9,
    prediction_processed: 10,
    done: 11
  };

  private _regEx = {
    lettersAndNumbers: /^[0-9a-zA-Z]+$/,
    nickname: /^[a-zA-Z0-9!@#$%\^:;"']{1,11}$/,
    nickname_reverse: /[^a-zA-Z0-9!@#$%\^:;."']/g,
    token_address_reverse: /[^a-zA-Z0-9]/g,
    phone : /^\d{7,20}$/,
    digits: /^\d+$/,
    digits_reverse : /\D/g,
    // tslint:disable-next-line:max-line-length
    email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    englishLettersOnly: /^['a-zA-Z ]+$/,
    // tslint:disable-next-line:quotemark
    lettersOnly: "^['\\p{L} ]+$"
  };

  get skinMap() {
    return this._skinMap;
  }

  get responseCodeMap() {
    return this._responseCodeMap;
  }

  get predictionStates() {
    return this._predictionStates;
  }

  get regEx() {
    return this._regEx;
  }

  get isLogged() {
    return this._isLogged;
  }

  get uploadTypes() {
    return {
      allowed: ['image/jpeg', 'image/png', 'application/pdf'],
      readable: '.jpeg, .png, .pdf'
    };
  }

}
