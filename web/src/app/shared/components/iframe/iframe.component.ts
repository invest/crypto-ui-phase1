import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit {
  @Input() info;
  @Output() complete = new EventEmitter();

  private iframe;

  constructor() { }

  ngOnInit() {
    this.iframe = (<HTMLIFrameElement>document.getElementById('the-frame'));

    this.iframe.src = this.info.url;

    this.checkIframe();
  }

  checkIframe() {
    let iframeUrl = '';
    try {
      iframeUrl = this.iframe.contentDocument.location.href;
    } catch (e) {}

    if (iframeUrl.toLowerCase().indexOf(this.info.checkFor) >= 0) {
      this.complete.emit({data: this.getUrlValues(iframeUrl)});
    } else {
      setTimeout(() => {
        this.checkIframe();
      }, 500);
    }
  }

  getUrlValues(url) {
    let params = window.location.search;

    if (url) {
      params = url.split('?');
      if (params.length > 1) {
        params = '?' + params[1];
      } else {
        return {};
      }
    }

    const query = params.substring(1);
    const vars = query.split('&');

    const rtn = {};

    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      rtn[pair[0]] = pair[1];
    }

    return rtn;
  }
}
