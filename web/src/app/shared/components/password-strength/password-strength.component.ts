import { Component, Input } from '@angular/core';

import { UtilsService } from '../../services/utils.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'password-strength',
  templateUrl: './password-strength.component.html',
  styleUrls: ['./password-strength.component.scss']
})
export class PasswordStrengthComponent {
  @Input('input') input;

  constructor(
    private utils: UtilsService
  ) { }

  checkPassword(value: string) {
    return this.utils.calcPasswordStrength(value);
  }
}
