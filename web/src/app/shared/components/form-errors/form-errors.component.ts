import { Component, Input } from '@angular/core';

/*
  input can be also the form
*/
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'form-errors',
  templateUrl: './form-errors.component.html',
  styleUrls: ['./form-errors.component.scss']
})
export class FormErrorsComponent {
  @Input('input') input;
}
