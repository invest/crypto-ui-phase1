import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  constructor(
    private translate: TranslateService
  ) { }

  transform(milSeconds: any, format?: any): any {
    const counterInfo = {
      months: 0,
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0
    };
    const secMS = 1000;
    const minMS = secMS * 60;
    const hourMS = minMS * 60;
    const dayMS = hourMS * 24;
    const monthMS = dayMS * 30;

    if (isNaN(milSeconds) || milSeconds === 0) {
      return '';
    }

    const translation = {
      month: 'month',
      months: 'months',
      day: 'day',
      days: 'days',
      hour: 'hour',
      hours: 'hours',
      minute: 'minute',
      minutes: 'minutes',
    };

    // return this.translate
    //  .get(['month', 'day', 'hour', 'minute'])
    // .subscribe();

    counterInfo.months = Math.floor(milSeconds / monthMS);

    const days = milSeconds - (counterInfo.months * monthMS);
    counterInfo.days = Math.floor(days / dayMS);

    const hours = days - (counterInfo.days * dayMS);
    counterInfo.hours = Math.floor(hours / hourMS);

    const minutes = hours - (counterInfo.hours * hourMS);
    counterInfo.minutes = Math.floor(minutes / minMS);

    const seconds = minutes - (counterInfo.minutes * minMS);
    counterInfo.seconds = Math.floor(seconds / secMS);


    if (format === 'verShort') {
      if (counterInfo.months > 0) {
        const text = (counterInfo.months === 1) ? translation.month : translation.months;
        return counterInfo.months + ' ' + text;
      } else if (counterInfo.days > 0) {
        const text = (counterInfo.days === 1) ? translation.day : translation.days;
        return counterInfo.days + ' ' + text;
      } else if (counterInfo.hours > 0) {
        const text = (counterInfo.hours === 1) ? translation.hour : translation.hours;
        return counterInfo.hours + ' ' + text;
      } else if (counterInfo.minutes > 0) {
        const text = (counterInfo.minutes === 1) ? translation.minute : translation.minutes;
        return counterInfo.minutes + ' ' + text;
      }
    } else {
      let text = '';

      if (counterInfo.months > 0) {
        text += counterInfo.months + ' ';
        // text += (counterInfo.months == 1) ? translation.month : translation.months;
      }

      if (counterInfo.days > 0 || text !== '') {
        text += (text !== '') ? '/' : '';
        text += counterInfo.days + ' ';
        // text += (counterInfo.days == 1) ? translation.day : translation.days;
      }

      if (counterInfo.hours > 0 || text !== '') {
        text += (text !== '') ? ' ' : '';
        text += counterInfo.hours;
        // text += (counterInfo.hours == 1) ? translation.hour : translation.hours;
      }

      if (counterInfo.minutes > 0 || text !== '') {
        text += (text !== '') ? ':' : '';
        text += counterInfo.minutes;
        // text += (counterInfo.minutes == 1) ? translation.minute : translation.minutes;
      }
      return text;
    }

  }
}
