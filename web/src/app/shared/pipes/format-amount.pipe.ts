import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatAmount'
})
export class FormatAmountPipe implements PipeTransform {

  transform(amount: any, args?: any): any {
    if (isNaN(amount)) {
      amount = 0;
    }

    return (Math.round((amount / 100) * 100) / 100).toFixed(2);
  }

}
