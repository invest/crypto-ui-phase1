import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { FormErrorsComponent } from './components/form-errors/form-errors.component';
import { IframeComponent } from './components/iframe/iframe.component';
import { PasswordStrengthComponent } from './components/password-strength/password-strength.component';
import { ImgToSvgDirective } from './directives/img-to-svg.directive';
import { RestrictCryptoDirective } from './directives/input/restrict-crypto.directive';
import { RestrictDigitsDirective } from './directives/input/restrict-digits.directive';
import { RestrictFloatDirective } from './directives/input/restrict-float.directive';
import { RestrictLettersDirective } from './directives/input/restrict-letters.directive';
import { RestrictNicknameDirective } from './directives/input/restrict-nickname.directive';
import { RestrictTokenAddressDirective } from './directives/input/restrict-token-address.directive';
import { SetErrorClassDirective } from './directives/set-error-class.directive';
import { SetInputStatesDirective } from './directives/set-input-states.directive';
import { FormatAmountPipe } from './pipes/format-amount.pipe';
import { FormatTimePipe } from './pipes/format-time.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { GeoIpService } from './services/geo-ip.service';
import { HttpService } from './services/http.service';
import { InfoService } from './services/info.service';
import { ResponseHandlerService } from './services/response-handler.service';
import { SettingsService } from './services/settings.service';
import { UtilsService } from './services/utils.service';
import { TranslateDirective } from './directives/translate.directive';
import { ShowPasswordDirective } from './directives/input/show-password.directive';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild({})
  ],
  exports: [
    FormErrorsComponent,
    PasswordStrengthComponent,
    IframeComponent,

    SetErrorClassDirective,
    SetInputStatesDirective,
    ImgToSvgDirective,
    RestrictDigitsDirective,
    RestrictFloatDirective,
    RestrictCryptoDirective,
    RestrictLettersDirective,
    RestrictNicknameDirective,
    TranslateDirective,
    ShowPasswordDirective,

    KeysPipe,
    FormatAmountPipe,
    FormatTimePipe
  ],
  providers: [
    GeoIpService,
    UtilsService,
    SettingsService,
    ResponseHandlerService,
    HttpService,
    InfoService
  ],
  declarations: [
    FormErrorsComponent,

    KeysPipe,
    FormatAmountPipe,
    FormatTimePipe,

    SetErrorClassDirective,
    SetInputStatesDirective,
    ImgToSvgDirective,
    RestrictDigitsDirective,
    RestrictFloatDirective,
    RestrictCryptoDirective,
    RestrictLettersDirective,
    RestrictNicknameDirective,
    RestrictTokenAddressDirective,
    PasswordStrengthComponent,
    IframeComponent,
    TranslateDirective,
    ShowPasswordDirective
  ]
})
export class SharedModule { }
