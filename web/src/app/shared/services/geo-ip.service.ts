import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class GeoIpService {
  private ipInfo;

  constructor(
    private http: Http,
  ) { }

  public getIp(): Promise<any> {
    if (!this.ipInfo) {
      return this.http
        .get('https://geoip.nekudo.com/api/')
        .map(data => this.ipInfo = data.json())
        .toPromise()
        .catch(this.handleError);
    } else {
      return new Promise((resolve) => resolve(this.ipInfo));
    }
  }

  get getIpStatic() {
    return this.ipInfo;
  }

  get locale() {
    if (!this.ipInfo) {
      return null;
    } else {
      return this.ipInfo.country.code.toLowerCase();
    }
  }

  private handleError(): Promise<any> {
    const ipInfo = {
      city: 'Unknown',
      country: {
        name: 'Unknown',
        code: 'Unknown'
      }
    };
    return new Promise((resolve) => resolve(ipInfo));
  }
}
