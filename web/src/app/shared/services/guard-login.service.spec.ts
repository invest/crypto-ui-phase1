import { TestBed, inject } from '@angular/core/testing';

import { GuardLogin } from './guard-login.service';

describe('GuardLogin', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GuardLogin]
    });
  });

  it('should be created', inject([GuardLogin], (service: GuardLogin) => {
    expect(service).toBeTruthy();
  }));
});
