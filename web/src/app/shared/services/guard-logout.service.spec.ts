import { TestBed, inject } from '@angular/core/testing';

import { GuardLogout } from './guard-logout.service';

describe('GuardLogout', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GuardLogout]
    });
  });

  it('should be created', inject([GuardLogout], (service: GuardLogout) => {
    expect(service).toBeTruthy();
  }));
});
