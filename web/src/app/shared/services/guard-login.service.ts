import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AppSettings } from './../app-settings';
import { LocalizeRouterService } from 'localize-router';

@Injectable()
export class GuardLogin implements CanActivate {
  constructor(
    // tslint:disable-next-line:no-shadowed-variable
    private AppSettings: AppSettings,
    private router: Router,
    private localize: LocalizeRouterService
  ) { }

  canActivate() {
    if (!this.AppSettings.isLogged) {
      return true;
    } else {
      const translatedPath: any = this.localize.translateRoute('/platform');
      this.router.navigate([translatedPath]);
      return false;
    }
  }
}
