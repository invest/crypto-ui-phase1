import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';

import { SettingsService } from './settings.service';


@Injectable()
export class StartupService {

  constructor(
      // private geoIp: GeoIpService,
      private settings: SettingsService
  ) {}

  // This is the method you want to call at bootstrap
  // Important: It should return a Promise
  load(): Promise<any> {
    return this.settings.get();
  }
}
