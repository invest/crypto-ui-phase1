import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { AppSettings } from '../app-settings';

@Injectable()
export class UtilsService {

  constructor(
    private appSettings: AppSettings
  ) {}

  searchJsonKeyInArray(arr: any[], key: any, val: any): number {
    if (arr) {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i][key] === val) {
          return i;
        }
      }
    }
    return -1;
  }

  searchJsonKeyInJson(obj, key, val) {
    if (obj) {
      for (const el in obj) {
        if (obj[el][key] === val) {
          return el;
        }
      }
    }
    return -1;
  }

  getUrlValue(key: string): string {
    const params = window.location.search;
    const query = params.substring(1);
    const vars = query.split('&');

    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      if (key === pair[0]) {
        return decodeURIComponent(pair[1]);
      }
    }
    return null;
  }

  get jsonLink() {
    return environment.jsonLink;
  }

  get javaLink() {
    return environment.javaLink;
  }

  get getOutgoingLinks() {
    return environment.outgoingLinks;
  }

  get isLocal() {
    return environment.isLocal;
  }

  get isPoduction() {
    return environment.production;
  }

  get supportEmail() {
    return environment.supportEmail;
  }

  get wp_url() {
    let url = this.getOutgoingLinks.homePage;

    if (this.appSettings.locale !== 'en') {
      url += this.appSettings.locale + '/';
    }

    return url;
  }

  // for some reason in ng 2+ there is no merge method - WTF
  merge(obj1, obj2) {
    for ( const key in obj2 ) {
      if ( obj2.hasOwnProperty(key) ) {
        if ( obj2[key].constructor === Object ) {
          obj1[key] = this.merge(obj1[key], obj2[key]);
        } else {
          obj1[key] = obj2[key];
        }
      }
    }

    return obj1;
  }

  parent(el, cls: string) {
    while (el) {
      el = el.parentElement;
      if (el.classList.contains(cls)) {
        return el;
      }
    }

    return null;
  }

  range(start, end) {
    const result = [];

    if (start < end) {
      for (let i = start; i <= end; i++) {
        if (i < 10) {
          result.push({
            withZero: '0' + i,
            value: i
          });
        } else {
          result.push({
            withZero: i.toString(),
            value: i
          });
        }
      }
    } else {
      for (let i = start; i >= end; i--) {
        if (i < 10) {
          result.push({
            withZero: '0' + i,
            value: i
          });
        } else {
          result.push({
            withZero: i.toString(),
            value: i
          });
        }
      }
    }
    return result;
  }

  calcPasswordStrength(password) {
    const strength = {
      goodEnough: false,
      dspl: ''
    };

    const strongRegex = new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$', 'g');
    const mediumRegex = new RegExp('^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$', 'g');
    const enoughRegex = new RegExp('(?=.{6,}).*', 'g');

    if (false === enoughRegex.test(password)) {
      strength.dspl = 'weak';
    } else if (strongRegex.test(password)) {
        strength.dspl =  'strong';
        strength.goodEnough =  true;
    } else if (mediumRegex.test(password)) {
      strength.dspl =  'medium';
      strength.goodEnough =  true;
    } else {
      strength.dspl =  'weak';
    }

    return strength;
  }

  addScript(src, id) {
    if (!document.getElementById(id)) {
      const s = document.createElement('script');
      s.setAttribute('src', src);
      s.id = id;
      document.body.appendChild(s);
    }
  }

  exportCSV(tableId) {
    let dataToExport = '"sep=;"\n';
    const hiddenColumns = [];

    const tableRows = document.getElementById(tableId).querySelectorAll('.row');

    for (let i = 0; i < tableRows.length; i++) {
      const cols = tableRows[i].querySelectorAll('.col');

      for (let j = 0; j < cols.length; j++) {
        dataToExport += (<HTMLElement>cols[j]).innerText.replace(/\r?\n|\r/g, '') + ';';
      }

      dataToExport += '\n';
    }

    this.download(dataToExport, 'download.csv', 'data:text/csv;charset=utf-8');
  }

  download(strData, strFileName, strMimeType) {
    const D = document;
    const a = D.createElement('a');
    strMimeType = strMimeType || 'application/octet-stream';

    if (navigator.msSaveBlob) { // IE10
        return navigator.msSaveBlob(new Blob([strData], {type: strMimeType}), strFileName);
    } /* end if(navigator.msSaveBlob) */

    if ('download' in a) { // html5 A[download]
      a.href = 'data:' + strMimeType + ',' + encodeURIComponent(strData);
      a.setAttribute('download', strFileName);
      a.innerHTML = 'downloading...';
      D.body.appendChild(a);
      setTimeout(function() {
        a.click();
        D.body.removeChild(a);
      }, 66);
      return true;
    } /* end if('download' in a) */

    // do iframe dataURL download (old ch+FF):
    const f = D.createElement('iframe');
    D.body.appendChild(f);
    f.src = 'data:' +  strMimeType   + ',' + encodeURIComponent(strData);

    setTimeout(function() {
        D.body.removeChild(f);
    }, 333);

    return true;
  }
}
