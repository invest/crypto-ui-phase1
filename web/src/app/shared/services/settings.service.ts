import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { environment } from '../../../environments/environment';
import { AppSettings } from '../app-settings';
import { UserService } from '../../user/services/user.service';

@Injectable()
export class SettingsService {
  public userInfo;

  constructor(
      private translate: TranslateService,
      private http: HttpClient,
      private appSettings: AppSettings
  ) { }

  public get(): Promise<any> {
    const storage = window.localStorage;
    /*
      TODO maybe if fixed in ng
      this is a bug in angular - on refresh route.snapshot is null
      https://github.com/angular/angular/issues/13829
      const forceSkinId = +this.searchJsonKeyInJson(this.skinMap, 'id', this.route.snapshot.paramMap.get('skinId'));
    */
    const forceLocale = this.indexOfNull(this.appSettings.locales, this.getUrlValue('locale'));
    const cookieLocale = this.indexOfNull(this.appSettings.locales, storage.getItem('LOCALIZE_DEFAULT_LANGUAGE'));
    // TODO this do not work - it loads before the init of translations
    const urlSkinLocale = this.indexOfNull(this.appSettings.locales, this.translate.currentLang);

    let locale = null;

    if (forceLocale) {
      locale = forceLocale;
    } else if (cookieLocale) {
      locale = cookieLocale;
    } else {
      if (urlSkinLocale) {
        locale = urlSkinLocale;
      }
    }

    const request = {
      locale: locale,
      forceLocale: forceLocale,
      cookieLocale: cookieLocale,
      urlSkinLocale: urlSkinLocale
    };

    const headers = new HttpHeaders()
      .set('Authorization', this.appSettings.settings.authorization)
      .set('Accept', 'application/json');

    const url = environment.jsonLink + 'user_personal_details/get_user_details_for_web';

    return this.http
      .post(url, JSON.stringify(request), {headers: headers, withCredentials: true})
      .toPromise()
      .then(data => {
        this.appSettings._isLogged = true;

        this.userInfo = data;

        if (data['data']['lang']) {
          locale = data['data']['lang'];
        }

        if (!locale) {
          locale = this.appSettings.locale;
        }

        storage.setItem('LOCALIZE_DEFAULT_LANGUAGE', locale);

        this.appSettings.locale = locale;

        return data;
      })
      .catch(data => {
        if (data.error) {
          this.appSettings._isLogged = false;
        }
      });
  }

  indexOfNull(list, search) {
    const index = list.indexOf(search);

    return (index > -1) ? search : null;
  }

  getUrlValue(key: string): string {
    const params = window.location.search;
    const query = params.substring(1);
    const vars = query.split('&');

    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      if (key === pair[0]) {
        return decodeURIComponent(pair[1]);
      }
    }
    return null;
  }
}
