import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { LocalizeRouterService } from 'localize-router';

import { AppSettings } from '../app-settings';
import { UtilsService } from './utils.service';


@Injectable()
export class ResponseHandlerService {

  constructor(
    private toaster: ToasterService,
    private translate: TranslateService,
    private appSettings: AppSettings,
    private router: Router,
    private localize: LocalizeRouterService,
    private utils: UtilsService
  ) {}

  check(respJson, thisRef?: any, formRef?: FormGroup): Promise<any> {
    let toasterTitleKey: string;
    let hasError: Boolean = true;
    let toasterNeeded: Boolean = true;

    // no response = network error
    if (!respJson) {
      respJson = {responseCode: this.appSettings.responseCodeMap.server_error};
    } else {
      if (respJson.error) {
        if (respJson.error.error) {
          respJson.responseCode = respJson.error.error.code === 0 ? 10002 : respJson.error.error.code;
          respJson.responseMsg = respJson.error.error.message;
        } else {
          respJson.responseCode = respJson.error.code === 0 ? 10002 : respJson.error.code;
          respJson.responseMsg = respJson.error.message;
        }
      }

      // if there is data - this is success. Move one.
      if (respJson.data || !respJson.status) {
        if (!respJson.data) {
          respJson = {
            data: respJson
          };
        }
        hasError = false;
      // if session is expired - redirect to login page
      } else if (respJson.status === this.appSettings.responseCodeMap.not_logged) {
        this.appSettings._isLogged = false;

        if (this.utils.isLocal) {
          const translatedPath: any = this.localize.translateRoute('/sign-in');
          this.router.navigate([translatedPath]);
        } else {
          window.location.replace(this.utils.getOutgoingLinks.homePage + '?showLogin=true');
        }
      } else {
        // if we have the form we check and add errors to specific inputs
        // if not, add them to the form itself
        if (formRef) {
          toasterNeeded = false;
          let formErrorsList;

          if (respJson.status === 422) {
            const errors = respJson.error.error;

            for (const key of Object.keys(errors)) {
              for (const error of errors[key]) {
                const input = formRef.get(key);

                const errorObj = {};

                errorObj[error] = respJson.error.error.properties ? respJson.error.error.properties : true;

                if (input) {
                  input.setErrors(errorObj);
                } else {
                  formErrorsList = {
                    ...formErrorsList,
                    ...errorObj
                  };
                }
              }
            }
          } else {
            const errorObj = {};

            if (respJson.responseMsg) {
              errorObj[respJson.responseCode] = {msg: respJson.responseMsg};
            } else {
              errorObj[respJson.responseCode] = respJson.properties ? respJson.properties : true;
            }

            formErrorsList = {
              ...formErrorsList,
              ...errorObj
            };
          }

          if (formErrorsList) {
            formRef.setErrors(formErrorsList);
          }
        }
      }
    }


    if (hasError && toasterNeeded) {
      toasterTitleKey = 'error-' + respJson.responseCode;

      this.translate
      .get(['error', toasterTitleKey])
      .subscribe(data => {
        // timeout is in case of refresh and there is no html container yet
        setTimeout(() => {
            this.toaster.pop({
              type: 'error',
              title: data.error,
              body: data[toasterTitleKey]
            });
          }, 0);
        });
    }

    if (hasError) {
      return new Promise((resolve, reject) => reject(respJson));
    } else {
      return new Promise((resolve) => resolve(respJson));
    }
  }
}
