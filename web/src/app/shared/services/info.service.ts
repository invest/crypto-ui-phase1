import { Injectable } from '@angular/core';

import { HttpService } from './http.service';

@Injectable()
export class InfoService {
  private _info;
  private isPlatformActive = true;
  private timer;

  constructor(
    private http: HttpService
  ) { }

  getInfo(): Promise<any> {
    if (this._info) {
      return new Promise((resolve) => resolve(this._info));
    } else {
      const request = {
        'countries_full': true,
        'ip2country': true,
        'country2currency': true
      };

      return this.http
        .getPost('info', request, 'data')
        .then(data => {
          this._info = data;
          return data;
        })
        .catch(data => {
          throw new Error(data);
        });
      }
  }

  getPlatformStatus() {
    if (!this.timer) {
      return this.http
       .get('get-platform-status', 'data')
       .then(data => {
         this.isPlatformActive = data.is_deposit_allowed;

          this.timer = setTimeout(() => {
            this.getPlatformStatus();
          }, 60000);
       });
    }
  }

  get isActive() {
    return this.isPlatformActive;
  }
}
