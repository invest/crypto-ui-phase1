import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { AppSettings } from '../app-settings';

@Injectable()
export class GuardLogout {
  constructor(
    // tslint:disable-next-line:no-shadowed-variable
    private AppSettings: AppSettings,
    private router: Router,
    private localize: LocalizeRouterService
  ) { }

  canActivate() {
    if (this.AppSettings.isLogged) {
      return true;
    } else {
      const translatedPath: any = this.localize.translateRoute('/sign-in');
      this.router.navigate([translatedPath]);
      return false;
    }
  }
}
