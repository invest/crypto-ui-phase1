import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ResponseHandlerService } from './response-handler.service';
import { UtilsService } from './utils.service';
import { AppSettings } from '../app-settings';

@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient,
    private utils: UtilsService,
    private resp: ResponseHandlerService,
    private appSettings: AppSettings
  ) { }

  get(serviceName: string, returnProperty?: string): Promise<any> {
    const headers = new HttpHeaders()
      .set('Authorization', this.appSettings.settings.authorization)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json');


    return this.http
      .get(this.utils.jsonLink + serviceName, {headers: headers, withCredentials: true})
      // .map(response => response.json())
      .toPromise()
      .then(data => this.resp.check(data))
      .then(data => {
        if (returnProperty) {
          return data[returnProperty];
        } else {
          return data;
        }
      })
      .catch(data => this.resp.check(data));
  }

  getPost(serviceName: string, request: any = {}, returnProperty?: string): Promise<any> {
    const headers = new HttpHeaders()
      .set('Authorization', this.appSettings.settings.authorization)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json');


    return this.http
      .post(this.utils.jsonLink + serviceName, JSON.stringify(request), {headers: headers, withCredentials: true})
      // .map(response => response.json())
      .toPromise()
      .then(data => this.resp.check(data))
      .then(data => {
        if (returnProperty) {
          return data[returnProperty];
        } else {
          return data;
        }
      })
      .catch(data => this.resp.check(data));
  }

  post(serviceName: string, thisRef, formRef?: FormGroup, override?): Promise<any> {
    let request;
    let form: FormGroup;

    thisRef.inAction = true;

    if (formRef) {
      form = formRef;
    } else {
      form = thisRef.form;
    }

    request = form.value;

    if (override) {
      request = {...request, ...override};
    }

    const headers = new HttpHeaders()
      .set('Authorization', this.appSettings.settings.authorization)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json');

    return this.http
      .post(this.utils.jsonLink + serviceName, JSON.stringify(request), {headers: headers, withCredentials: true})
      .toPromise()
      .then(data => {
        thisRef.inAction = false;
        return this.resp.check(data, thisRef, form);
      })
      .catch(data => {
        thisRef.inAction = false;
        return this.resp.check(data, thisRef, form);
      });
  }
}
