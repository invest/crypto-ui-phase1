import { Injectable } from '@angular/core';
import { Response, ResponseOptions } from '@angular/http';
import { ErrorHandler } from 'protractor/built/exitCodes';

// import { ResponseHandlerService } from './services/response-handler.service';
import { AppSettings } from './app-settings';


@Injectable()
export class AppErrorHandler implements ErrorHandler {
  constructor(
    // private resp: ResponseHandlerService,
    private appSettings: AppSettings
  ) { }

  handleError(error) {
    const options = new ResponseOptions({
      body: { responseCode: this.appSettings.responseCodeMap.network_error }
    });

    const res = new Response(options);

    // TODO why the fuck this do not work it does not want to show the toester
    // this.resp.check(res);
  }
}
