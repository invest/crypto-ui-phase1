import { Directive, ElementRef, HostListener, Input, OnInit, Renderer } from '@angular/core';

import { UtilsService } from './../services/utils.service';

@Directive({
  selector: '[appSetInputStates]'
})
export class SetInputStatesDirective implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('appSetInputStates') className;

  private element;

  constructor(
    private el: ElementRef,
    private utils: UtilsService,
    private renderer: Renderer
  ) { }

  @HostListener('focus') onfocus() {
    this.toggleCLass('focused', true);
    this.toggleCLass('touched', true);
  }

  @HostListener('blur') onBlur() {
    this.toggleCLass('focused', false);

    if (this.el.nativeElement.value === '') {
      this.toggleCLass('touched', false);
    }
  }

  ngOnInit() {
    this.element = this.utils.parent(this.el.nativeElement, this.className);
  }

  toggleCLass(className: string, flag: boolean) {
    this.renderer.setElementClass(this.element, className, flag);
  }
}
