import { Directive, DoCheck, ElementRef, Input, Renderer } from '@angular/core';

@Directive({
  selector: '[appSetErrorClass]'
})
export class SetErrorClassDirective implements  DoCheck  {
  // tslint:disable-next-line:no-input-rename
  @Input('appSetErrorClass') input;

  constructor(
    private el: ElementRef,
    private renderer: Renderer
  ) {}

  ngDoCheck() {
    this.check();
  }

  check() {
    if (this.input.valid) {
      this.toggleCLass('correct', true);
      this.toggleCLass('incorrect', false);
    } else if (this.input.touched && this.input.dirty && this.input.invalid) {
      this.toggleCLass('correct', false);
      this.toggleCLass('incorrect', true);
    }
  }

  toggleCLass(className: string, flag: boolean) {
    this.renderer.setElementClass(this.el.nativeElement, className, flag);
  }

}
