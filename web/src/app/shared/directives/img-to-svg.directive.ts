import { Http } from '@angular/http';
import { Directive, ElementRef, OnInit, Input, Renderer2, Injectable } from '@angular/core';

@Injectable()
@Directive({
  selector: '[appImgToSvg]'
})
export class ImgToSvgDirective implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('appImgToSvg') svg;

  constructor(
    private el: ElementRef,
    private http: Http,
    private render: Renderer2
  ) { }

  ngOnInit() {
    this.render.addClass(this.el.nativeElement, 'svg-from-file');

    this.http
      .get(this.svg)
      .map(response => response['_body'])
      .subscribe(data => {
        this.render.setProperty(this.el.nativeElement, 'innerHTML', data);
      });
  }

}
