import { AppSettings } from '../../app-settings';
import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appRestrictDigits]'
})
export class RestrictDigitsDirective {
  constructor(
    private el: ElementRef,
    private appSettings: AppSettings
  ) { }

  @HostListener('keyup') onKeyUp() {
    const value = this.el.nativeElement.value;

    this.el.nativeElement.value = value.replace(this.appSettings.regEx.digits_reverse, '');
  }
}
