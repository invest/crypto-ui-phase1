import { AppSettings } from '../../app-settings';
import { Directive, ElementRef, HostListener } from '@angular/core';
import XRegExp from 'xregexp';

@Directive({
  selector: '[appRestrictLetters]'
})
export class RestrictLettersDirective {
  constructor(
    private el: ElementRef,
    private appSettings: AppSettings
  ) { }

  @HostListener('keyup') onKeyUp() {
    let value = this.el.nativeElement.value;

    const regEx_letters_reverse = new XRegExp((this.appSettings.regEx.lettersOnly + '')
      .replace(/\//g, '').replace('^[', '[^').replace('+$', ''), 'g');

    value = value.replace(regEx_letters_reverse, '');

    this.el.nativeElement.value = value;
  }
}
