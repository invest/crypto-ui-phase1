import { Directive, ElementRef, HostListener } from '@angular/core';

import { AppSettings } from '../../app-settings';

@Directive({
  selector: '[appRestrictCrypto]'
})
export class RestrictCryptoDirective {
  constructor(
    private el: ElementRef,
    private appSettings: AppSettings
  ) { }

  @HostListener('keyup') onKeyUp() {
    let value = this.el.nativeElement.value;
    const val = value.replace(',', '.').split('.');

    value = val[0].replace(this.appSettings.regEx.digits_reverse, '');

    if (val[1] || val[1] === '') {
      val[1] = val[1].replace(this.appSettings.regEx.digits_reverse, '');
      if (val[1].length > 2) {
        val[1] = val[1].substr(0, 8);
      }
      value += '.' + val[1];
    }
    this.el.nativeElement.value = value;
  }
}
