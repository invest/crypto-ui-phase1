import { AfterViewInit, Directive, ElementRef, OnDestroy, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appShowPassword]'
})
export class ShowPasswordDirective implements AfterViewInit, OnDestroy {

  private input: HTMLInputElement;
  listenClickFunc: Function;

  constructor(
    private element: ElementRef,
    private renderer: Renderer2
  ) { }

  ngAfterViewInit() {
    this.input = this.element.nativeElement.parentElement.querySelectorAll('input')[0];

    this.listenClickFunc = this.renderer.listen(this.element.nativeElement, 'click', (event) => {
      event.preventDefault();

      this.input.type = (this.input.type === 'password') ? 'text' : 'password';
    });

  }

  ngOnDestroy() {
    if (this.listenClickFunc) {
      this.listenClickFunc();
    }
  }

}
