import { Directive, ElementRef, HostListener } from '@angular/core';

import { AppSettings } from '../../app-settings';

@Directive({
  selector: '[appRestrictTokenAddress]'
})
export class RestrictTokenAddressDirective {
  constructor(
    private el: ElementRef,
    private appSettings: AppSettings
  ) { }

  @HostListener('keyup') onKeyUp() {
    const value = this.el.nativeElement.value;

    this.el.nativeElement.value = value.replace(this.appSettings.regEx.token_address_reverse, '');
  }
}
