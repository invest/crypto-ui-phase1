import { Input, Directive, ElementRef, Renderer2, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalizeRouterService } from 'localize-router';

@Directive({
  selector: '[appTranslate]'
})
export class TranslateDirective implements AfterViewInit, OnDestroy {
  @Input() appTranslate: string;
  @Input() translateParams: string;
  listenClickFunc: Function;

  constructor(
    private element: ElementRef,
    private translate: TranslateService,
    private router: Router,
    private renderer: Renderer2,
    private localize: LocalizeRouterService,
  ) { }

  ngAfterViewInit() {
    this.translate.get(this.appTranslate, JSON.parse(this.translateParams))
      .subscribe((translateText: string) => {
        this.element.nativeElement.innerHTML = translateText;

        const navigationElements = Array.prototype.slice.call(this.element.nativeElement.querySelectorAll('a[routerLink]'));

        for (const elem of navigationElements) {
          this.listenClickFunc = this.renderer.listen(elem, 'click', (event) => {
            event.preventDefault();

            const translatedPath: any = this.localize.translateRoute(elem.getAttribute('routerLink'));
            this.router.navigate([translatedPath]);
          });
        }
      });
  }

  ngOnDestroy() {
    if (this.listenClickFunc) {
      this.listenClickFunc();
    }
  }
}
