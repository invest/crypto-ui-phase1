export interface User {
  id: number;
  account_id: number;
  email: string;
  displayName: string;
  first_name: string;
  last_name: string;
  active: boolean;
  birth_date: string;
  address: string;
  street_no: string;
  city: string;
  zip: string;
  mobile_number: string;
  country: string;
  currency: string;
  isCurrencySet: boolean;
}
