import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password-view',
  templateUrl: './forgot-password-view.component.html',
  styleUrls: ['./forgot-password-view.component.scss']
})
export class ForgotPasswordViewComponent {
  showMsg: Boolean = false;
  resetEmail = '';

  constructor() { }

  finishForgotPassword(data) {
    this.showMsg = true;
    this.resetEmail = data.resetEmail;
  }
}
