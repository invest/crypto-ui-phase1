import { Component } from '@angular/core';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent {

  public isChangePasswordFinished = false;
  public isChangePhoneFinished = false;
  public supportEmail: string;

  constructor(
    private utils: UtilsService
  ) {
    this.supportEmail = this.utils.supportEmail;
  }

  finishChangePassword(data) {
    this.isChangePasswordFinished = true;
  }

  finishChangePhone(data) {
    this.isChangePhoneFinished = true;
  }
}

