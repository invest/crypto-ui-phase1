import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProofOfResidenceComponent } from './proof-of-residence.component';

describe('ProofOfResidenceComponent', () => {
  let component: ProofOfResidenceComponent;
  let fixture: ComponentFixture<ProofOfResidenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProofOfResidenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProofOfResidenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
