import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { DocumentsService } from '../../services/documents.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit, OnDestroy {

  public documentsList = [];
  private sub: ISubscription;

  constructor(
    private router: Router,
    private localize: LocalizeRouterService,
    private documents: DocumentsService
  ) {
    this.documentsList = this.documents.documentsList;
  }

  ngOnInit() {
    this.documents.goToNextStep(false, true);

    this.sub = this.router.events
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          const translatedPath = this.localize.translateRoute('/platform/documents');

          if (event.url === translatedPath) {
            this.documents.goToNextStep(false, true);
          }
        }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
