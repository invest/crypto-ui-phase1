import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { DocumentsService } from '../../../services/documents.service';

@Component({
  selector: 'app-proof-of-id-view',
  templateUrl: './proof-of-id-view.component.html',
  styleUrls: ['./proof-of-id-view.component.scss']
})
export class ProofOfIdViewComponent {

  constructor(
    private documents: DocumentsService
  ) { }

  stepFinished(data) {
    this.documents.goToNextStep();
  }
}
