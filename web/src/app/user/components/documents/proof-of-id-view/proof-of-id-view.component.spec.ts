import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProofOfIdViewComponent } from './proof-of-id-view.component';

describe('ProofOfIdViewComponent', () => {
  let component: ProofOfIdViewComponent;
  let fixture: ComponentFixture<ProofOfIdViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProofOfIdViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProofOfIdViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
