import { Component } from '@angular/core';

import { DocumentsService } from './../../../services/documents.service';
import { LocalizeRouterService } from 'localize-router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personal-info-view',
  templateUrl: './personal-info-view.component.html',
  styleUrls: ['./personal-info-view.component.scss']
})
export class PersonalInfoViewComponent {

  constructor(
    private documents: DocumentsService
  ) { }

  stepFinished(data) {
    this.documents.goToNextStep();
  }

}
