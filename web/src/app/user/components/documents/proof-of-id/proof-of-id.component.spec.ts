import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProofOfIdComponent } from './proof-of-id.component';

describe('ProofOfIdComponent', () => {
  let component: ProofOfIdComponent;
  let fixture: ComponentFixture<ProofOfIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProofOfIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProofOfIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
