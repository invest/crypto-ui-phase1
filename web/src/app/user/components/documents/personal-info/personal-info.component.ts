import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';

import { AppSettings } from '../../../../shared/app-settings';
import { InfoService } from '../../../../shared/services/info.service';
import { ResponseHandlerService } from '../../../../shared/services/response-handler.service';
import { UtilsService } from '../../../../shared/services/utils.service';
import { DocumentsService } from '../../../services/documents.service';
import { UserService } from '../../../services/user.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {
  @Output() complete = new EventEmitter();
  @ViewChild('inputFront') inputFrontRef: ElementRef;

  public inAction = false;
  public range: (start: any, end: any) => any[];
  public year_18 = new Date().getFullYear() - 18;
  public countries;
  public ipCountry;

  public uploadComplated = false;
  public fileFront: UploadFile;
  uploadInputFront: EventEmitter<UploadInput>;
  public fileFrontUploaded = false;
  options: UploaderOptions = {
    concurrency: 1,
    allowedContentTypes: this.appSettings.uploadTypes.allowed
  };

  constructor(
    private documents: DocumentsService,
    private utils: UtilsService,
    private info: InfoService,
    private userService: UserService,
    private appSettings: AppSettings,
    private resp: ResponseHandlerService,
  ) {
    this.uploadInputFront = new EventEmitter<UploadInput>();

    this.range = utils.range;
  }

  form = new FormGroup({
    fname: new FormControl({
      value: this.userService.user.first_name,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    lname: new FormControl({
      value: this.userService.user.last_name,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    birth_date: new FormControl({
      value: this.userService.user.birth_date,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    birth_day: new FormControl({
      value: '',
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    birth_month: new FormControl({
      value: '',
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    birth_year: new FormControl({
      value: '',
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    addr: new FormControl({
      value: this.userService.user.address,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    city: new FormControl({
      value: this.userService.user.city,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    zip: new FormControl({
      value: this.userService.user.zip,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    mobile_number: new FormControl({
      value: this.userService.user.mobile_number,
      disabled: this.isComplate
    }, [
      Validators.required,
      Validators.minLength(7)
    ]),
    phone_prefix: new FormControl({
      value: '',
      disabled: true
    }, [
      Validators.required
    ]),
    country: new FormControl({
      value: this.userService.user.country,
      disabled: this.isComplate
    }, [
      Validators.required
    ]),
    politically_exposed_person: new FormControl({
      value: true,
      disabled: this.isComplate
    }, [
      Validators.pattern('true')
    ]),
    ustax: new FormControl({
      value: true,
      disabled: this.isComplate
    }, [
      Validators.pattern('true')
    ]),
    front: new FormControl('')
  });

  ngOnInit() {
    this.info.getInfo()
      .then(data => {
        this.countries = data.countries_full;
        this.ipCountry = data.ip2country;

        this.setPhonePrefixById();

        this.setBirthDate();
      });
  }

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (!this.isComplate) {
      // check if form valid
      if (this.form.invalid) {return; }

      const override = {
        mobile_number: this.inp('phone_prefix').value + ' ' + this.inp('mobile_number').value
      };

      // submit the form
      this.documents.savePersonalInfo(this, override)
        .then(data => {
          this.documents.documentsList[0].status = 3;

          this.userService.setUserProperties({
            first_name: this.inp('fname').value,
            last_name: this.inp('lname').value,
            birth_date: this.inp('birth_date').value,
            address: this.inp('addr').value,
            city: this.inp('city').value,
            zip: this.inp('zip').value,
            mobile_number: this.inp('mobile_number').value,
            country: this.inp('country').value
          });

          this.complete.emit(data);
        });
      } else {
        this.complete.emit({});
      }
  }

  // TODO move to service or someting
  checkBirthDay() {
    const day = this.inp('birth_day').value;
    const month = this.inp('birth_month').value;
    const year = this.inp('birth_year').value;

    const stringDate = year + '-' + month + '-' + day;
    const daysInMonth = new Date(year, month, 0).getDate();
    const birthDate = new Date(stringDate);
    const legalDate = new Date(birthDate.setFullYear(birthDate.getFullYear() + 18));
    const date = new Date();

    if (day === '' || month === '' || year === '') {
      return;
    }

    this.form.controls['birth_date'].markAsDirty();
    this.form.controls['birth_date'].markAsTouched();

    // clear errors
    this.form.controls['birth_day'].updateValueAndValidity();

    if (daysInMonth < day) {
      // error: not existing day in the month
      this.form.controls['birth_day'].setErrors({
        notExistingDay: {
          daysInMonth: daysInMonth
        }
      });
    } else if (legalDate > date) {
      // error: not  18
      this.form.controls['birth_date'].setErrors({
        minimalAge: {
          age: 18
        }
      });
    } else {
      this.form.controls['birth_date'].setValue(stringDate);
    }
  }

  get isComplate() {
    return this.documents.documentsList[0].status > 0;
  }

  setPhonePrefix() {
    this.form.controls['phone_prefix'].setValue('+' + this.countries[this.inp('country').value].telcode);
  }

  setPhonePrefixById() {
    // set phone prefix by ip detect
    if (!this.userService.user.country) {
      this.inp('phone_prefix').setValue('+' + this.countries[this.ipCountry].telcode);
      this.inp('country').setValue(this.countries[this.ipCountry].iso);
    } else {
      this.inp('phone_prefix').setValue('+' + this.countries[this.userService.user.country].telcode);
    }
  }

  setBirthDate() {
    const birth_date = new Date(parseInt(this.userService.user.birth_date, 10) * 1000);

    if (!isNaN(birth_date.getTime())) {
      const birth_day = birth_date.getDate() < 10 ? '0' + birth_date.getDate() : birth_date.getDate();
      this.inp('birth_day').setValue(birth_day.toString());

      const birth_month = birth_date.getMonth() < 9 ? '0' + (birth_date.getMonth() + 1) : birth_date.getMonth() + 1;
      this.inp('birth_month').setValue(birth_month.toString());

      this.inp('birth_year').setValue(birth_date.getFullYear().toString());
    }
  }

  get submitButton() {
    return this.documents.showFinish ? 'button-finish' : 'button-next';
  }

  get isFormDisabled() {
    return this.documents.showFinish ? (this.documents.generalDocumentStatus === 2) : (this.form.invalid || this.inAction);
  }

  // upload file start here
  onUploadOutputFront(output: UploadOutput): void {
    if (output.file && output.file.size > 3000000) {// ~3MB
      this.setFileSizeError('front', output.file.type);
    } else if (output.type === 'allAddedToQueue') {
      if (this.inp('front').value !== '') {
        this.documents.saveVerifyIdentity({
          uploadInput: this.uploadInputFront,
          file: this.fileFront,
          fileType: 'Utility bill',
          serviceName: 'save_verify_residential'
        });
      }
    } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') {
        if (this.fileFront && this.fileFront.id !== '') {
          this.uploadInputFront.emit({ type: 'remove', id: this.fileFront.id });
        }
        this.fileFront = output.file;
        this.inp('front').setValue(output.file.name);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      this.fileFront = output.file;
    } else if (output.type === 'removed') {
      this.fileFront = output.file;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      this.setFileError('front', output.file.type);
    } else if (output.type === 'done' && typeof output.file !== 'undefined') {
      this.uploadInputFront.emit({ type: 'remove', id: this.fileFront.id });

      this.resp.check(output.file.response, this, this.form)
        .then(data => {
          this.fileFrontUploaded = true;
          this.checkAllUploaded();
        })
        .catch(data => {
          this.uploadAgain('front', false);
          this.checkAllUploaded();
        });

      this.checkAllUploaded();
    }
  }

  checkAllUploaded() {
    if (this.fileFrontUploaded) {
      this.uploadComplated = true;
      return true;
    } else {
      this.uploadComplated = false;
    }

    return false;
  }

  setFileError(input, fileType) {
    this.form.controls[input].markAsDirty();
    this.form.controls[input].markAsTouched();

    this.form.controls[input].setErrors({
      incorrectType: {
        allowedContentTypes: this.appSettings.uploadTypes.readable,
        currentFileType: fileType
      }
    });
  }

  setFileSizeError(input, fileType) {
    this.form.controls[input].markAsDirty();
    this.form.controls[input].markAsTouched();

    this.form.controls[input].setErrors({
      fileSizeLImit: true
    });
  }

  uploadAgain(type, setValue: boolean = true) {
    this.fileFrontUploaded = false;
    this.fileFront = null;
    this.inputFrontRef.nativeElement.value = '';

    if (setValue) {
      this.inp(type).setValue('');
    }
  }
}
