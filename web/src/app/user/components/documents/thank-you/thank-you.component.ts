import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DocumentsService } from '../../../services/documents.service';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.scss']
})
export class ThankYouComponent implements OnInit {
  public documentsList = [];

  showThankYou = false;

  constructor(
    private documents: DocumentsService,
    private route: Router
  ) {
    this.documentsList = this.documents.documentsList;
  }

  ngOnInit() {
    this.showThankYou = this.route.url.indexOf('thankyou') > -1;
  }

}
