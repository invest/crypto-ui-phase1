import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { DocumentsService } from '../../../services/documents.service';

@Component({
  selector: 'app-selfie-view',
  templateUrl: './selfie-view.component.html',
  styleUrls: ['./selfie-view.component.scss']
})
export class SelfieViewComponent {

  constructor(
    private documents: DocumentsService
  ) { }

  stepFinished(data) {
    this.documents.goToNextStep();
  }

}
