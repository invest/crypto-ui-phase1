import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfieViewComponent } from './selfie-view.component';

describe('SelfieViewComponent', () => {
  let component: SelfieViewComponent;
  let fixture: ComponentFixture<SelfieViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfieViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfieViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
