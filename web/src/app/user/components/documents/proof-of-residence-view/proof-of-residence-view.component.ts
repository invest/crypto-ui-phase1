import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { DocumentsService } from '../../../services/documents.service';

@Component({
  selector: 'app-proof-of-residence-view',
  templateUrl: './proof-of-residence-view.component.html',
  styleUrls: ['./proof-of-residence-view.component.scss']
})
export class ProofOfResidenceViewComponent {

  constructor(
    private documents: DocumentsService
  ) { }

  stepFinished(data) {
    this.documents.goToNextStep();
  }

}
