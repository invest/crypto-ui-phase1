import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProofOfResidenceViewComponent } from './proof-of-residence-view.component';

describe('ProofOfResidenceViewComponent', () => {
  let component: ProofOfResidenceViewComponent;
  let fixture: ComponentFixture<ProofOfResidenceViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProofOfResidenceViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProofOfResidenceViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
