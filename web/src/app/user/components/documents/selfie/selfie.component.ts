import { Component, EventEmitter, OnInit, Output, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';

import { AppSettings } from '../../../../shared/app-settings';
import { ResponseHandlerService } from '../../../../shared/services/response-handler.service';
import { DocumentsService } from '../../../services/documents.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'selfie',
  templateUrl: './selfie.component.html',
  styleUrls: ['./selfie.component.scss']
})
export class SelfieComponent implements OnInit {
  @Output() complete = new EventEmitter();
  @ViewChild('inputFront') inputFrontRef: ElementRef;

  public inAction = false;
  public uploadComplated = false;
  public skipUpload = false;
  public formDisabled = true;
  public typesList = [];

  public fileFront: UploadFile;
  uploadInputFront: EventEmitter<UploadInput>;
  public fileFrontUploaded = false;
  options: UploaderOptions = {
    concurrency: 1,
    allowedContentTypes: this.appSettings.uploadTypes.allowed
  };


  constructor(
    private documents: DocumentsService,
    private resp: ResponseHandlerService,
    private appSettings: AppSettings
  ) {
    this.typesList = this.documents.idTypesList;

    this.uploadInputFront = new EventEmitter<UploadInput>();
  }

  form = new FormGroup({
    type: new FormControl({
      value: '',
      disabled: this.isDocComplate
    }, [
      Validators.required
    ]),
    front: new FormControl('', [
      Validators.required
    ])
  });

  ngOnInit() {
    this.documents.getFileStatus()
      .then(() => {
        for (const type of this.typesList) {
          if (type.selfieStatus.fileName !== '') {
            // check of we already have document with pending status and skip rejected
            if (this.inp('type').value &&
                parseInt(this.inp('type').value.selfieStatus.status, 10) === 1 &&
                parseInt(type.selfieStatus.status, 10) === 3) {
              continue;
            }

            this.inp('type').setValue(type);
            this.resetFileUpload();

            if (parseInt(type.selfieStatus.status, 10) === 2) {
              break;
            }
          }
        }
      });
  }

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (!this.skipUpload) {
      if (this.form.valid) {
        const type = this.inp('type').value.value;

        this.inAction = true;

        this.documents.saveVerifyIdentity({
          uploadInput: this.uploadInputFront,
          file: this.fileFront,
          fileType: 'Selfie ' + type,
          serviceName: 'save_verify_identity'
        });
      }
    } else {
      this.complete.emit({});
    }
  }

  onUploadOutputFront(output: UploadOutput): void {
    if (output.file && output.file.size > 3000000) {// ~3MB
      this.setFileSizeError('front', output.file.type);
    } else if (output.type === 'allAddedToQueue') {
      this.submit();
    } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') {
        if (this.fileFront && this.fileFront.id !== '') {
          this.uploadInputFront.emit({ type: 'remove', id: this.fileFront.id });
        }
        this.fileFront = output.file;
        this.inp('front').setValue(output.file.name);

        if (this.inp('type').value === '') {
          this.inp('type').setValue(this.typesList[0]);
        }
        this.inp('type').value.selfieStatus.fileName = output.file.name;
        this.inp('type').value.selfieStatus.status = 1;
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      this.fileFront = output.file;
    } else if (output.type === 'removed') {
      this.fileFront = output.file;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      this.setFileError('front', output.file.type);
    } else if (output.type === 'done' && typeof output.file !== 'undefined') {
      this.uploadInputFront.emit({ type: 'remove', id: this.fileFront.id });

      this.resp.check(output.file.response, this, this.form)
        .then(data => {
          this.fileFrontUploaded = true;
          this.checkAllUploaded();

          this.inp('type').value.selfieStatus.oldFileName = this.inp('type').value.selfieStatus.fileName;
          this.inp('type').value.selfieStatus.oldStatus = 1;
        })
        .catch(data => {
          this.inAction = false;
          this.uploadAgain('front', false);
          this.checkAllUploaded();
        });
    }
  }

  uploadAgain(type, setValue: boolean = true) {
    this.fileFrontUploaded = false;
    this.fileFront = null;
    this.inputFrontRef.nativeElement.value = '';

    if (setValue) {
      this.inp(type).setValue('');
      this.inp('type').value.selfieStatus.fileName = '';
      this.inp('type').value.selfieStatus.status = 0;
    }
  }

  checkAllUploaded() {
    if (this.fileFrontUploaded) {
      this.inAction = false;
      this.uploadComplated = true;

      this.documents.documentsList[2].status = 1;

      return true;
    } else {
      this.uploadComplated = false;
    }

    return false;
  }

  setFileError(input, fileType) {
    this.form.controls[input].markAsDirty();
    this.form.controls[input].markAsTouched();

    this.form.controls[input].setErrors({
      incorrectType: {
        allowedContentTypes: this.appSettings.uploadTypes.readable,
        currentFileType: fileType
      }
    });
  }

  setFileSizeError(input, fileType) {
    this.form.controls[input].markAsDirty();
    this.form.controls[input].markAsTouched();

    this.form.controls[input].setErrors({
      fileSizeLImit: true
    });
  }

  getFile() {
    if (this.inp('type').value === '') {
      return {};
    } else {
      return this.inp('type').value.selfieStatus;
    }
  }

  resetFileUpload() {
    this.inp('front').setValue('');
    this.inp('type').value.selfieStatus.fileName = this.inp('type').value.selfieStatus.oldFileName || '';
    this.inp('type').value.selfieStatus.status = this.inp('type').value.selfieStatus.oldStatus || 0;
  }


  get submitButton() {
    this.skipUpload = true;

    const selfieRef = this.inp('type').value.selfieStatus;

    let textkey: string;
    let disabled: boolean;

    if (this.documents.showFinish) {
      textkey = 'button-finish';
      disabled = (this.documents.generalDocumentStatus === 2);
    } else if (this.uploadComplated) {
      textkey = 'button-next';
      disabled = false;
    } else if (selfieRef && selfieRef.fileName !== '' && selfieRef.fileName === selfieRef.oldFileName) {
      textkey = 'button-next';
      disabled = false;
    } else {
      this.skipUpload = false;
      disabled = (this.form.invalid || this.inAction);
      textkey = 'button-next';
    }


    // angular for some reason does not want to refresh the disabled status of the submit button - this is a hack to do the job
    if (this.formDisabled !== disabled) {
      setTimeout(() => {
        this.formDisabled = disabled;
      }, 0);
    }

    return textkey;
  }

  get isFormDisabled() {
    return this.formDisabled;
  }

  get isDocComplate() {
    return this.documents.isDocumentComplate(2);
  }

  checkType() {
    if (this.inp('type').invalid) {
      this.inp('type').markAsDirty();
      this.inp('type').markAsTouched();
    }
  }
}
