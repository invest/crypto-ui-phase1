import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { LocalizeRouterService } from 'localize-router';

@Component({
  selector: 'app-reset-password-view',
  templateUrl: './reset-password-view.component.html',
  styleUrls: ['./reset-password-view.component.scss']
})
export class ResetPasswordViewComponent {
  constructor(
    private router: Router,
    private localize: LocalizeRouterService,
    private toaster: ToasterService,
    private translate: TranslateService,
  ) { }

  finishResetPassword() {
    this.translate
      .get('reset-password-toaster')
      .subscribe(data => {
        this.toaster.pop({
          type: 'info',
          body: data
        });
      });

    const translatedPath: any = this.localize.translateRoute('/sign-in');
    this.router.navigate([translatedPath]);
  }

}
