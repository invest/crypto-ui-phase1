import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../services/user.service';
import { PasswordValidator } from '../../../shared/validators/Password.validators';
import { UtilsService } from '../../../shared/services/utils.service';
import { InfoService } from '../../../shared/services/info.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'change-phone-form',
  templateUrl: './change-phone.component.html',
  styleUrls: ['./change-phone.component.scss']
})
export class ChangePhoneComponent {
  @Output() complete = new EventEmitter();

  public inAction = false;
  public countries;
  public ipCountry;

  constructor(
    private userService: UserService,
    private utils: UtilsService,
    private info: InfoService
  ) {
    this.info.getInfo()
      .then(data => {
        this.countries = data.countries_full;
        this.ipCountry = data.ip2country;

        this.setPhonePrefixById();
      });
  }

  form = new FormGroup({
    phone_number: new FormControl(this.userService.user.mobile_number, [
      Validators.required,
      Validators.minLength(7)
    ]),
    phone_prefix: new FormControl({
      value: '',
      disabled: true
    }, [
      Validators.required
    ])
  });

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (this.form.valid) {
      const override = {
        phone_number: this.inp('phone_prefix').value + ' ' + this.inp('phone_number').value
      };

      this.userService
        .changePhone(this, override)
        .then(data => {
          this.userService.setUserProperties({
            mobile_number: this.inp('phone_number').value
          });

          this.complete.emit(data);
        });
      }
  }

  setPhonePrefixById() {
    // set phone prefix by ip detect
    if (!this.userService.user.country) {
      this.inp('phone_prefix').setValue('+' + this.countries[this.ipCountry].telcode);
    } else {
      this.inp('phone_prefix').setValue('+' + this.countries[this.userService.user.country].telcode);
    }
  }
}
