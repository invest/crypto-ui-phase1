import { Component } from '@angular/core';

import { DocumentsService } from '../../services/documents.service';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.scss']
})
export class RegisterViewComponent {
  constructor(
    private documents: DocumentsService
  ) { }

  finishRegister(data) {
    this.documents.goToNextStep();
  }
}
