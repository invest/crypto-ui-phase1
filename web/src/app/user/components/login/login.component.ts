import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AppSettings } from '../../../shared/app-settings';
import { UtilsService } from '../../../shared/services/utils.service';
import { UserService } from './../../services/user.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  @Output() complete = new EventEmitter();
  @ViewChild('captchaRef') captchaRef;

  private savedUserName = window.localStorage.getItem('userName');
  public inAction: Boolean = false;
  public reCaptchKey: string;

  constructor(
    private user: UserService,
    private appSettings: AppSettings,
    private utils: UtilsService
  ) {
    this.reCaptchKey = appSettings.settings.recaptchaKey;

    // if (!this.utils.isPoduction) {
    //   this.inp('g-recaptcha-response').setValue('fake stuff');
    // }
  }

  form = new FormGroup({
    username: new FormControl(this.savedUserName, [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ]),
    'g-recaptcha-response': new FormControl('', [
      Validators.required
    ]),
    remember: new FormControl(true)
  });

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (this.form.valid) {
      this.user
        .login(this)
        .then(data => {
          this.complete.emit(data);
        })
        .catch(() => {
          setTimeout(() => {
            this.captchaRef.reset();
          }, 2000);
        });
      }
  }

  resolved(captchaResponse: string) {
    this.inp('g-recaptcha-response').setValue(captchaResponse);
  }
}
