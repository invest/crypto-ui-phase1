import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { TransactionsService } from '../../../platform/services/transactions.service';
import { DocumentsService } from '../../services/documents.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent {
  constructor(
    private documents: DocumentsService
  ) { }

  finishLogin(data) {
    this.documents.goToNextStep(true, true);
  }
}
