import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { AppSettings } from './../../../shared/app-settings';
import { UserService } from './../../services/user.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'reset-password-form',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {
  @Output() complete = new EventEmitter();

  inAction: Boolean = false;

  constructor(
    private user: UserService,
    private appSettings: AppSettings,
    private router: ActivatedRoute
  ) { }

  form = new FormGroup({
    newPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ]),
    token: new FormControl(this.router.snapshot.paramMap.get('token'), [
      Validators.required
    ])
  });

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (this.form.valid) {
      this.user
        .resetPassword(this)
        .then(data => {
          this.complete.emit(data);
        });
      }
  }
}
