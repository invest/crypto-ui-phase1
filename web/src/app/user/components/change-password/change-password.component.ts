import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../services/user.service';
import { PasswordValidator } from '../../../shared/validators/Password.validators';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'change-password-form',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {
  @Output() complete = new EventEmitter();

  inAction: Boolean = false;

  constructor(
    private user: UserService,
    private utils: UtilsService
  ) { }

  form = new FormGroup({
    old_password: new FormControl('', [
      Validators.required,
      Validators.minLength(7),
      PasswordValidator.passwordStrength(this.utils)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(7),
      PasswordValidator.passwordStrength(this.utils)
    ]),
    password_confirmation: new FormControl('', [
      Validators.required,
      Validators.minLength(7),
      PasswordValidator.matchInputs,
      PasswordValidator.passwordStrength(this.utils)
    ])
  });

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (this.form.valid) {
      this.user
        .changePassword(this)
        .then(data => {
          this.complete.emit(data);
        });
      }
  }
}
