import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

import { AppSettings } from './../../../shared/app-settings';
import { UserService } from './../../services/user.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'forgot-password-form',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  @Output() complete = new EventEmitter();

  inAction: Boolean = false;

  constructor(
    private user: UserService,
  ) {}

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ])
  });

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    if (this.form.valid) {
      this.user
        .forgotPassword(this)
        .then(data => {
          this.complete.emit(data);
        });
      }
  }
}
