import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AppSettings } from '../../../shared/app-settings';
import { PasswordValidator } from '../../../shared/validators/Password.validators';
import { UserService } from './../../services/user.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'register-form',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  @Output() complete = new EventEmitter();
  @ViewChild('captchaRef') captchaRef;

  public inAction: Boolean = false;
  public reCaptchKey: string;

  constructor(
    private user: UserService,
    private appSettings: AppSettings,
    private utils: UtilsService
  ) {
    this.reCaptchKey = appSettings.settings.recaptchaKey;

    // if (!this.utils.isPoduction) {
    //   this.inp('g-recaptcha-response').setValue('fake stuff');
    // }
  }

  form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      PasswordValidator.passwordStrength(this.utils)
    ]),
    password_confirmation: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      PasswordValidator.matchInputs
    ]),
    'g-recaptcha-response': new FormControl('', [
      Validators.required
    ]),
    acceptTerms: new FormControl(false, [
      Validators.requiredTrue
    ])
  });

  inp(element) {
    return this.form.get(element);
  }

  get outgoingLinks() {
    return this.utils.getOutgoingLinks;
  }

  submit() {
    if (this.form.valid) {
      const override = {
        locale: this.appSettings.locale
      };

      this.user
        .register(this, override)
        .then(data => {
          this.complete.emit(data);
        })
        .catch(() => {
          setTimeout(() => {
            this.captchaRef.reset();
          }, 2000);
        });
      }
  }

  resolved(captchaResponse: string) {
    this.inp('g-recaptcha-response').setValue(captchaResponse);
  }
}
