import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';
import { NgUploaderModule } from 'ngx-uploader';

import { GuardLogin } from './../shared/services/guard-login.service';
import { SharedModule } from './../shared/shared.module';
import { DocumentsComponent } from './components/documents/documents.component';
import { PersonalInfoViewComponent } from './components/documents/personal-info-view/personal-info-view.component';
import { PersonalInfoComponent } from './components/documents/personal-info/personal-info.component';
import { ProofOfIdViewComponent } from './components/documents/proof-of-id-view/proof-of-id-view.component';
import { ProofOfIdComponent } from './components/documents/proof-of-id/proof-of-id.component';
import { ProofOfResidenceViewComponent } from './components/documents/proof-of-residence-view/proof-of-residence-view.component';
import { ProofOfResidenceComponent } from './components/documents/proof-of-residence/proof-of-residence.component';
import { SelfieViewComponent } from './components/documents/selfie-view/selfie-view.component';
import { SelfieComponent } from './components/documents/selfie/selfie.component';
import { ForgotPasswordViewComponent } from './components/forgot-password-view/forgot-password-view.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { LoginComponent } from './components/login/login.component';
import { PersonalDetailsComponent } from './components/personal-details/personal-details.component';
import { RegisterViewComponent } from './components/register-view/register-view.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetPasswordViewComponent } from './components/reset-password-view/reset-password-view.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { DocumentsService } from './services/documents.service';
import { UserService } from './services/user.service';
import { ThankYouComponent } from './components/documents/thank-you/thank-you.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ChangePhoneComponent } from './components/change-phone/change-phone.component';

const routes = [
  { path: '', component: LoginViewComponent, canActivate: [GuardLogin] },
  { path: 'sign-in', component: LoginViewComponent, canActivate: [GuardLogin] },
  { path: 'sign-up', component: RegisterViewComponent, canActivate: [GuardLogin] },
  { path: 'forgot-password', component: ForgotPasswordViewComponent, canActivate: [GuardLogin] },
  { path: 'reset-password/:token', component: ResetPasswordViewComponent, canActivate: [GuardLogin] }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({}),
    LocalizeRouterModule.forChild(routes),
    RecaptchaModule,
    ReactiveFormsModule,
    NgUploaderModule,

    SharedModule
  ],
  providers: [
    UserService,
    DocumentsService
  ],
  declarations: [
    LoginViewComponent,
    LoginComponent,
    RegisterViewComponent,
    RegisterComponent,
    ForgotPasswordViewComponent,
    ForgotPasswordComponent,
    ResetPasswordViewComponent,
    ResetPasswordComponent,
    PersonalInfoComponent,
    PersonalInfoViewComponent,
    ProofOfIdComponent,
    ProofOfIdViewComponent,
    SelfieComponent,
    SelfieViewComponent,
    ProofOfResidenceViewComponent,
    ProofOfResidenceComponent,
    DocumentsComponent,
    PersonalDetailsComponent,
    ThankYouComponent,
    VerifyEmailComponent,
    ChangePasswordComponent,
    ChangePhoneComponent
  ],
  exports: [
    PersonalInfoComponent,
    PersonalInfoViewComponent,
    ProofOfIdComponent,
    ProofOfIdViewComponent,
    SelfieComponent,
    SelfieViewComponent,
    ProofOfResidenceViewComponent,
    ProofOfResidenceComponent,
    DocumentsComponent,
    PersonalDetailsComponent,
  ],
  entryComponents: [
    VerifyEmailComponent
  ]
})
export class UserModule { }
