import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';
import { UploadInput } from 'ngx-uploader';

import { AppSettings } from '../../shared/app-settings';
import { HttpService } from '../../shared/services/http.service';
import { ResponseHandlerService } from '../../shared/services/response-handler.service';
import { SettingsService } from '../../shared/services/settings.service';
import { UtilsService } from '../../shared/services/utils.service';
import { TransactionsService } from '../../platform/services/transactions.service';

@Injectable()
export class DocumentsService {

  private serviceDir = '';
  private isStatusesDone = false;

  /*
    statuses
      -1 required
      -2 rejected
      1 pending
      2 done
  */
  public documentsList = [
    {
      translation: 'menu-personal-information',
      status: -1,
      link: '/platform/documents/personal-info',
      classIcon: 'personal-info'
    }, {
      translation: 'menu-proof-of-id',
      status: -1,
      link: '/platform/documents/proof-of-id',
      classIcon: 'proof-of-id'
    }, {
      translation: 'menu-selfie',
      status: -1,
      link: '/platform/documents/selfie',
      classIcon: 'selfie'
    // }, {
    //   translation: 'menu-proof-of-residence',
    //   status: -1,
    //   link: '/platform/documents/proof-of-residence',
    //   classIcon: 'proof-of-residence'
    }
  ];

  /*
    1, pending
    2, accepted
    3, decline
  */
  public idTypesList = [
    {
      translation: 'id-passport',
      value: 'Passport',
      proofStatus: {
        front: {
          fileName: '',
          status: 0,
          oldFileName: '',
          oldStatus: 0
        },
        back: {
          fileName: '',
          status: 0,
          oldFileName: '',
          oldStatus: 0
        }
      },
      selfieStatus: {
        fileName: '',
        status: 0,
        oldFileName: '',
        oldStatus: 0
      }
    }, {
      translation: 'id-national-id',
      value: 'National ID',
      proofStatus: {
        front: {
          fileName: '',
          status: 0,
          oldFileName: '',
          oldStatus: 0
        },
        back: {
          fileName: '',
          status: 0,
          oldFileName: '',
          oldStatus: 0
        }
      },
      selfieStatus: {
        fileName: '',
        status: 0,
        oldFileName: '',
        oldStatus: 0
      }
    }
  ];

  public residenceDocsList = [
    {
      translation: 'residence-utility-bill',
      value: 'Utility bill',
      status: {
        fileName: '',
        status: 0,
        oldFileName: '',
        oldStatus: 0
      }
    }, {
      translation: 'residence-bank-statement',
      value: 'Bank statement',
      status: {
        fileName: '',
        status: 0,
        oldFileName: '',
        oldStatus: 0
      }
    }, {
      translation: 'residence-tax-bill',
      value: 'Tax bill',
      status: {
        fileName: '',
        status: 0,
        oldFileName: '',
        oldStatus: 0
      }
    }, {
      translation: 'residence-credit-card-statement',
      value: 'Credit card statement',
      status: {
        fileName: '',
        status: 0,
        oldFileName: '',
        oldStatus: 0
      }
    }
  ];

  private generalMap = {
    'Passport Front' : this.idTypesList[0].proofStatus.front,
    'Passport Back' : this.idTypesList[0].proofStatus.back,
    'Selfie Passport' : this.idTypesList[0].selfieStatus,
    'National ID Front' : this.idTypesList[1].proofStatus.front,
    'National ID Back' : this.idTypesList[1].proofStatus.back,
    'Selfie National ID' : this.idTypesList[1].selfieStatus,
    'Utility bill' : this.residenceDocsList[0].status,
    'Bank statement' : this.residenceDocsList[1].status,
    'Tax bill' : this.residenceDocsList[2].status,
    'Credit card statement' : this.residenceDocsList[3].status
  };

  constructor(
    private http: HttpService,
    private utils: UtilsService,
    private resp: ResponseHandlerService,
    private appSettings: AppSettings,
    private router: Router,
    private localize: LocalizeRouterService,
    private settings: SettingsService,
    private transactions: TransactionsService
  ) { }

  nextStep(showStatus?: boolean) {
    for (const document of this.documentsList) {
      if (document.status < 0) {
        return document.link;
      }
    }

    return '/platform/documents/' + ((showStatus) ? 'status' : 'thankyou');
  }

  goToNextStep(skipThankYou?: boolean, goToStatus?: boolean) {
    let url: string;

    if (this.generalDocumentStatus === 2 && skipThankYou) {
      this.transactions.getCryptoReport(1)
      .then(resp => {
        if (resp.data.length && resp.data[0].status !== '0' && resp.data[0].status !== '30') {
          url = '/platform/orders';
        } else {
          url = '/platform/buy-crypto';
        }

        const translatedPathIn = this.localize.translateRoute(url);
        this.router.navigate([translatedPathIn]);
      });
    } else {
      url = this.nextStep(goToStatus);
      if (this.generalDocumentStatus !== 2) {
        this.transactions.openOrdersCount = 0;
      }
    }

    const translatedPath = this.localize.translateRoute(url);
    this.router.navigate([translatedPath]);
  }

  savePersonalInfo(thisRef, override?) {
    return this.http
      .post(this.serviceDir + 'user_personal_details/save_user_details', thisRef, null, override)
      .then(data => {
        return data;
      });

  }

  // uploadInput, file, fileType
  saveVerifyIdentity(params): void {
    const event: UploadInput = {
      type: 'uploadFile',
      file: params.file,
      url: this.utils.jsonLink + 'user_compliance/' + params.serviceName,
      method: 'POST',
      headers: {
        'Authorization': this.appSettings.settings.authorizationUpload,
        'Accept': 'application/json'
      },
      withCredentials: true,
      data: {
        doc_type: params.fileType
      }
    };

    params.uploadInput.emit(event);
  }

  initAllProperties() {
    this.isStatusesDone = false;

    for (const doc of this.documentsList) {
      doc.status = -1;
    }

    this.clearOldStatuses();
  }

  initDocumentStatuses() {
    this.initAllProperties();

    // const STATUS_NOT_VERIFIED_ID = 10;
    // const STATUS_VERIFIED_ID = 11;
    // const STATUS_PENDING_ID = 13;
    const data = this.settings.userInfo.data;

    if (this.documentsList[0]) {
      this.documentsList[0].status = data.fname === '' ? -1 : 3;
    }

    if (this.documentsList[1]) {
      this.documentsList[1].status = this.checkInitDocStatus(
        parseInt(data.comp_status_identity_front, 10),
        parseInt(data.comp_status_identity_back, 10)
      );
    }

    if (this.documentsList[2]) {
      this.documentsList[2].status = this.checkInitDocStatus(
        parseInt(data.comp_status_selfie, 10)
      );
    }

    if (this.documentsList[3]) {
      this.documentsList[3].status = this.checkInitDocStatus(
        parseInt(data.comp_status_residential, 10)
      );
    }
  }

  checkInitDocStatus(file1, file2?): number {
    if (file2) {
      if (file1 === 10 || file2 === 10) {
        return -2;
      } else if (file1 === 1 || file2 === 1) {
        return -1;
      } else if (file1 === 13 || file2 === 13) {
        return 1;
      } else if (file1 === 11 || file2 === 11) {
        return 2;
      }
    } else {
      switch (file1) {
        case 1: return -1;
        case 10: return -2;
        case 11: return 2;
        case 13: return 1;
      }
    }
  }

  getFileStatus(): Promise<any> {
    if (!this.isStatusesDone) {
      return this.http
        .get(this.serviceDir + 'compliance-status/regulation-files')
          .then(data => {
            this.clearOldStatuses();

            for (let i = data.data.length - 1; i >= 0; i--) {
              if (this.generalMap[data.data[i].type]) {
                this.generalMap[data.data[i].type].fileName = data.data[i].org_file;
                this.generalMap[data.data[i].type].oldFileName = data.data[i].org_file;
                this.generalMap[data.data[i].type].status = data.data[i].status;
                this.generalMap[data.data[i].type].oldStatus = data.data[i].status;
              }
            }

            this.isStatusesDone = true;

            return;
          });
        } else {
          return new Promise((resolve) => resolve());
        }
  }

  clearOldStatuses() {
    for (const type of this.idTypesList) {
      type.proofStatus.front.status = type.proofStatus.front.oldStatus = 0;
      type.proofStatus.front.fileName = type.proofStatus.front.oldFileName = '';

      type.proofStatus.back.status = type.proofStatus.back.oldStatus = 0;
      type.proofStatus.back.fileName = type.proofStatus.back.oldFileName = '';

      type.selfieStatus.status = type.selfieStatus.oldStatus = 0;
      type.selfieStatus.fileName = type.selfieStatus.oldFileName = '';
    }
    for (const doc of this.residenceDocsList) {
      doc.status.status = doc.status.oldStatus = 0;
      doc.status.fileName =  doc.status.oldFileName = '';
    }
  }

  get generalDocumentStatus() {
    // 0-required; 1-pending; 2-finished
    let generalDocumentsStatus = 2;

    for (const doc of this.documentsList) {
      if (doc.status < generalDocumentsStatus) {
        generalDocumentsStatus = doc.status;
      }
    }

    return generalDocumentsStatus;
  }

  get showFinish() {
    return this.generalDocumentStatus > 0;
  }

  isDocumentComplate(id) {
    return (this.documentsList[id].status > 1);
  }

}
