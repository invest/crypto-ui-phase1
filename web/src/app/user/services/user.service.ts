import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { AppSettings } from '../../shared/app-settings';
import { HttpService } from '../../shared/services/http.service';
import { ResponseHandlerService } from '../../shared/services/response-handler.service';
import { SettingsService } from '../../shared/services/settings.service';
import { UtilsService } from '../../shared/services/utils.service';
import { VerifyEmailComponent } from '../components/verify-email/verify-email.component';
import { User } from './../../shared/interfaces/user';
import { DocumentsService } from './documents.service';

@Injectable()
export class UserService {
  private serviceDir = '';
  private _user: {};

  constructor(
    private http: HttpService,
    private utils: UtilsService,
    private resp: ResponseHandlerService,
    private appSettings: AppSettings,
    private router: Router,
    private localize: LocalizeRouterService,
    private settingsService: SettingsService,
    public dialog: MatDialog,
    public documents: DocumentsService
  ) { }

  initUser() {
    this.setUser(this.settingsService.userInfo.data);

    this.checkLogin();

    return this.user;
  }

  get user(): User {
    return <User>this._user;
  }

  set user(userObj) {
    this._user = userObj;
  }

  clearUser() {
    this._user = {};
    this.checkLogin();
  }

  login(thisRef) {
    return this.http
      .post(this.serviceDir + 'login', thisRef)
      .then(data => {
        this.settingsService.userInfo = data;

        this.setUser(data.data);

        this.checkLogin();

        if (thisRef.form.get('remember')) {
          window.localStorage.setItem('userName', thisRef.form.get('username').value);
        }

        return this.user;
      });
  }

  register(thisRef, override) {
    return this.http
      .post(this.serviceDir + 'registration', thisRef, null, override)
      .then(data => {
        this.settingsService.userInfo = data;

        this.setUser(data.data);

        this.checkLogin();

        return data.user;
      });
  }

  logout() {
    this.http
      .getPost(this.serviceDir + 'logout')
      .then(data => {
        if (this.utils.isLocal) {
          this.clearUser();
          const translatedPath: any = this.localize.translateRoute('/sign-in');
          this.router.navigate([translatedPath]);
        } else {
          window.location.replace(this.utils.getOutgoingLinks.homePage);
        }

        // return data;
      });
  }

  forgotPassword(thisRef) {
    return this.http
      .post(this.serviceDir + 'forgot', thisRef)
      .then(data => {
        data.resetEmail = thisRef.form.get('email').value;
        return data;
      });
  }

  resetPassword(thisRef) {
    return this.http
      .post(this.serviceDir + 'resetPassword', thisRef);
  }

  changePassword(thisRef) {
    return this.http
      .post('profile/change-password', thisRef);
  }

  changePhone(thisRef, override) {
    return this.http
      .post('profile/change-phone', thisRef, null, override);
  }

  setUser(userInfo) {
    let mobile_number = userInfo.tnp;

    if (mobile_number) {
      mobile_number = mobile_number.split(' ');
      mobile_number = mobile_number.length === 1 ? mobile_number[0] : mobile_number[1];
    } else {
      mobile_number = '';
    }

    this.user = {
      id: userInfo.id,
      account_id: userInfo.account_id,
      email: userInfo.email,
      displayName: (userInfo.full_name) ? userInfo.full_name.trim() : '',
      first_name: userInfo.fname || '',
      last_name: userInfo.lname || '',
      active: (userInfo.email_confirmed === '1'),
      birth_date: userInfo.birth_date || '',
      address: userInfo.addr || '',
      street_no: '',
      city: userInfo.city || '',
      zip: userInfo.zip || '',
      mobile_number: mobile_number,
      country: userInfo.country || '',
      currency: userInfo.currency || '',
      isCurrencySet: (userInfo.change_user_currency_popup_shown === '1')
    };
  }
  setUserProperties(userInfo) {
    for (const prop of Object.keys(userInfo)) {
      if (null !== this.user[prop]) {
        this._user[prop] = userInfo[prop];
      }
    }
  }

  private checkLogin() {
    if (this.user.id) {
      this.appSettings._isLogged = true;

      this.documents.initDocumentStatuses();

      if (!this.user.active) {
        this.dialog.open(VerifyEmailComponent, {
          disableClose: true,
          hasBackdrop: true,
          data: { email: this.user.email }
        });
       }
    } else {
      this.appSettings._isLogged = false;
    }
  }

  changeLanguage(locale) {
    const storage = window.localStorage;
    const index = this.appSettings.locales.indexOf(locale);

    if (index > -1) {
      this.localize.changeLanguage(locale);
      this.appSettings.locale = locale;

      const request = {
        lang: locale
      };

      this.http.getPost(this.serviceDir + 'languages/switch', request);
    }
  }
}
