import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';
import * as path from 'path';

import { CryptoService } from '../shared/services/crypto.service';
import { GuardLogout } from '../shared/services/guard-logout.service';
import { SharedModule } from '../shared/shared.module';
import { SupportComponent } from '../static/components/support/support.component';
import { PersonalInfoViewComponent } from '../user/components/documents/personal-info-view/personal-info-view.component';
import { ProofOfIdViewComponent } from '../user/components/documents/proof-of-id-view/proof-of-id-view.component';
import {
    ProofOfResidenceViewComponent,
} from '../user/components/documents/proof-of-residence-view/proof-of-residence-view.component';
import { ThankYouComponent } from '../user/components/documents/thank-you/thank-you.component';
import { PersonalDetailsComponent } from '../user/components/personal-details/personal-details.component';
import { StaticModule } from './../static/static.module';
import { DocumentsComponent } from './../user/components/documents/documents.component';
import { SelfieViewComponent } from './../user/components/documents/selfie-view/selfie-view.component';
import { BuyCryptoComponent } from './components/buy-crypto/buy-crypto.component';
import { Deposit3dComponent } from './components/deposit-3d/deposit-3d.component';
import { DepositSuccessBankwireComponent } from './components/deposit-success-bankwire/deposit-success-bankwire.component';
import { DepositSuccessCcComponent } from './components/deposit-success-cc/deposit-success-cc.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { BitrexService } from './services/bitrex.service';
import { CheckoutService } from './services/checkout.service';
import { DepositService } from './services/deposit.service';
import { TransactionsService } from './services/transactions.service';

const routes = [
  {
    path: 'platform',
    // data: { skipRouteLocalization: true },
    canActivate: [GuardLogout],
    children: [
      { path: 'buy-crypto', component: BuyCryptoComponent },
      { path: 'orders', component: TransactionsComponent },
      { path: 'personal-details', component: PersonalDetailsComponent },
      { path: 'support', component: SupportComponent },
      { path: 'documents', component: DocumentsComponent,
        children: [
          { path: 'personal-info', component: PersonalInfoViewComponent },
          { path: 'proof-of-id', component: ProofOfIdViewComponent },
          { path: 'selfie', component: SelfieViewComponent },
          { path: 'proof-of-residence', component: ProofOfResidenceViewComponent },
          { path: 'status', component: ThankYouComponent },
          { path: 'thankyou', component: ThankYouComponent }
        ]
     }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({}),
    LocalizeRouterModule.forChild(routes),
    ReactiveFormsModule,

    SharedModule,
    StaticModule
  ],
  declarations: [
    BuyCryptoComponent,
    TransactionsComponent,
    DepositSuccessBankwireComponent,
    Deposit3dComponent,
    DepositSuccessCcComponent
  ],
  providers: [
    DepositService,
    BitrexService,
    CheckoutService,
    TransactionsService,
    CryptoService
  ],
  entryComponents: [
    DepositSuccessBankwireComponent,
    Deposit3dComponent,
    DepositSuccessCcComponent
  ]
})
export class PlatformModule { }
