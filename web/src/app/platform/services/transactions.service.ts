import { Injectable } from '@angular/core';

import { HttpService } from '../../shared/services/http.service';

@Injectable()
export class TransactionsService {

  public resultLimit = 20;
  public resultsPerPage = 6;
  public openOrdersCount = 0;

  constructor(
    private http: HttpService
  ) { }


  getCryptoReport(page: number, limit?: number) {
    if (!limit) {
      limit = this.resultLimit;
    }
    const from = (page - 1) * limit;
    const to = from + limit;

    return this.http
      .get('crypto-trade?from=' + from + '&to=' + to)
      .then(data => {
        this.openOrdersCount = 0;

        for (const order of data.data) {
          if (order.status !== '0' && order.status !== '30') {
            this.openOrdersCount++;
          }
        }

        return data;
      });
  }

  splitOrders(orderList, page) {
    const openOrders = [];
    const historyOrders = [];

    const from = (page - 1) * this.resultsPerPage;
    let to = page * this.resultsPerPage;
    if (to > orderList.length) {
      to = orderList.length;
    }

    for (let i = from; i < to; i++) {
      const order = orderList[i];

      order.detailsOpened = false;
      order.amount = order.amount ? order.amount : 0;
      order.price_per_coin = order.price_per_coin ? parseFloat(order.price_per_coin) : '';

      if (order.status === '30' || order.status === '0') {
        historyOrders.push(order);
      } else {
        openOrders.push(order);
      }
    }

    return {
      open: openOrders,
      history: historyOrders
    };
  }

}
