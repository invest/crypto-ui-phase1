import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HttpService } from '../../shared/services/http.service';
import { UtilsService } from '../../shared/services/utils.service';

@Injectable()
export class BitrexService {

  constructor(
    private http: HttpClient,
    private httpService: HttpService,
    private utils: UtilsService
  ) { }

  getLevels(market): Promise<any> {
    const range = {
      from: 0,
      to: 0
    };

    // if (market === 'USDT-BTC') {
    //   range.from = 10000;
    //   range.to = 10300;
    // } else {
    //   range.from = 800;
    //   range.to = 850;
    // }

    // return new Promise((resolve) => resolve({
    //   bid: 911.10000000,
    //   ask: this.getRandomArbitrary(range.from, range.to),
    //   last: 911.10000000
    // }));

    return this.http
      .get(this.utils.javaLink + 'ticker1?market=' + market)
      .toPromise();
  }

  getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }
}
