import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { UtilsService } from '../../shared/services/utils.service';
import { UserService } from '../../user/services/user.service';

@Injectable()
export class CheckoutService {

  constructor(
    private utils: UtilsService,
    private userService: UserService
  ) { }


  init(): Promise<any> {
    return new Promise((resolve) => {
      if (!(<any>window).CKOConfig) {
        (<any>window).CKOConfig = {
          publicKey: environment.checkoutKey,
          customerEmail: this.userService.user.email,
          debugMode: !this.utils.isPoduction,
          ready: (event) => {
            resolve(event);
          }
        };

        this.utils.addScript(this.utils.getOutgoingLinks.checkoutUrl, 'checkout');
      } else {
        (<any>window).CheckoutKit.addEventHandler((<any>window).CheckoutKit.Events.READY, (e)  => {
          (<any>window).CheckoutKit.removeAllEventHandlers(true);

          resolve(event);
        });
      }
    });
  }

  addButtonEvent(id) {
    const inputs = document.getElementsByTagName('input');

    for (let i = inputs.length - 1; i >= 0; i--) {
        if (inputs[i].name.toLowerCase() === 'cko-public-key' || inputs[i].name.toLowerCase() === 'cko-card-token') {
            inputs[i].parentElement.removeChild(inputs[i]);
        }
    }

    (<HTMLButtonElement>document.getElementById('checkoutSubmit')).disabled = false;

    (<any>window).CheckoutKit.monitorForm('#' + id, (<any>window).CheckoutKit.CardFormModes.CARD_TOKENISATION);
  }

  onSubmit(): Promise<any> {
    return new Promise((resolve, reject) => {
      (<any>window).CheckoutKit.addEventHandler((<any>window).CheckoutKit.Events.CARD_TOKENISED, (e)  => {
        resolve(e.data);
      });

      (<any>window).CheckoutKit.addEventHandler((<any>window).CheckoutKit.Events.API_ERROR, (e)  => {
        reject(e);
      });
    });
  }
}
