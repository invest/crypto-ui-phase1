import { Injectable } from '@angular/core';
import { HttpService } from '../../shared/services/http.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilsService } from '../../shared/services/utils.service';
import { ResponseHandlerService } from '../../shared/services/response-handler.service';
import { AppSettings } from '../../shared/app-settings';

@Injectable()
export class DepositService {

  private serviceDir = '';

  public marketList = [
    {
      symbol: 'BTC',
      pear: 'USDT-BTC',
      title: 'Bitcoin'
    }, {
      symbol: 'ETH',
      pear: 'USDT-ETH',
      title: 'Ether'
    }
  ];

  public paymentMethodsList = [
    {
      translation: 'bank-wire',
      type: 'bank_transfer',
      fees: [],
      active: false
    }, {
      translation: 'credit-card',
      type: 'cardpay',
      fees: [],
      active: true
    }
  ];

  public currencyList = [
    {
      name: 'USD'
    }, {
      name: 'EUR'
    }, {
      name: 'GBP'
    }
  ];

  constructor(
    private http: HttpService,
    private httpClient: HttpClient,
    private utils: UtilsService,
    private resp: ResponseHandlerService,
    private appSettings: AppSettings
  ) { }

  // test cards
  // https://docs.checkout.com/getting-started/testing-and-simulating-charges/test-cards
  deposit(thisRef, override) {
    return this.http
      .post(this.serviceDir + 'crypto-trade', thisRef, null, override);
  }

  checkoutDeposit(thisRef, request) {
    const headers = new HttpHeaders()
    .set('Authorization', this.appSettings.settings.authorization)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json');

    return this.httpClient
      .post(this.utils.jsonLink + 'crypto-trade/checkout-deposit', JSON.stringify(request), {headers: headers, withCredentials: true})
      .toPromise()
      .then(data => {
        thisRef.inAction = false;
        return this.resp.check(data, thisRef, thisRef.form);
      })
      .catch(data => {
        thisRef.inAction = false;
        return this.resp.check(data, thisRef, thisRef.form);
      });
  }

  getDepositInfo() {
    return this.http
      .get('crypto-trade/rates')
        .then(data => {
          const rates = {};

          this.paymentMethodsList[0].fees = [
            {
              name: 'fee',
              value: (data.data.fees.bank_transfer / 100)
            }
          ];

          this.paymentMethodsList[1].fees = [
            {
              name: 'fee',
              value: (data.data.fees.credit_card / 100)
            }, {
              name: 'fee-clearance',
              value: (data.data.fees.clearance_fee / 100)
            }
          ];

          for (const rate of data.data.rates) {
            rates[rate.currency_pair] = rate.value;
          }

          return rates;
        });
  }

  changeUserCurrency(thisRef, override) {
    return this.http
      .post('TPAccount/change_user_currency', thisRef, null, override)
        .then(data => {
          return this.http
            .post('TPAccount/set_change_user_currency_popup_shown', thisRef, null, override);
        });
  }
}
