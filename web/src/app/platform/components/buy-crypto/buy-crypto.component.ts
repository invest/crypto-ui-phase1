import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { CryptoService } from '../../../shared/services/crypto.service';
import { InfoService } from '../../../shared/services/info.service';
import { UtilsService } from '../../../shared/services/utils.service';
import { BlockchainValidator } from '../../../shared/validators/blockchain.validators';
import { DocumentsService } from '../../../user/services/documents.service';
import { UserService } from '../../../user/services/user.service';
import { BitrexService } from '../../services/bitrex.service';
import { CheckoutService } from '../../services/checkout.service';
import { DepositService } from '../../services/deposit.service';
import { DepositSuccessBankwireComponent } from '../deposit-success-bankwire/deposit-success-bankwire.component';

@Component({
  selector: 'app-buy-crypto',
  templateUrl: './buy-crypto.component.html',
  styleUrls: ['./buy-crypto.component.scss']
})
export class BuyCryptoComponent implements OnInit, OnDestroy {

  public inAction = false;
  public levelsInterval;
  public activeInput = 'fiat';
  public currentCryptoRate;
  public fiatRates;
  public fee: number;
  public amount: number;
  public tx_key: string;
  public generalDocumentStatus;

  public selectedMarket;
  public marketList = [];
  public paymentMethodsList = [];
  public selectedPaymentMethod;
  public currencyList = [];

  constructor(
    private depositService: DepositService,
    private documents: DocumentsService,
    private bitrexService: BitrexService,
    public dialog: MatDialog,
    public utils: UtilsService,
    public userService: UserService,
    private checkout: CheckoutService,
    private localize: LocalizeRouterService,
    private router: Router,
    private crypto: CryptoService,
    public info: InfoService
  ) {
    this.marketList = depositService.marketList;
    this.paymentMethodsList = depositService.paymentMethodsList;
    this.currencyList = depositService.currencyList;
    this.generalDocumentStatus = documents.generalDocumentStatus;

    this.selectedMarket = this.marketList[0];
    this.inp('market').setValue(this.selectedMarket.pear);

    this.selectedPaymentMethod = this.paymentMethodsList[1];
    this.inp('method').setValue(this.selectedPaymentMethod.type);
  }

  form = new FormGroup({
    market: new FormControl('', [
      Validators.required
    ]),
    original_amount: new FormControl(500, [
      Validators.required
    ]),
    client_currency: new FormControl({
      value: 'EUR',
      // disabled: this.userService.user.isCurrencySet,
      disabled: true
    }, [
      Validators.required
    ]),
    quantity_client: new FormControl('', [
      Validators.required
    ]),
    rate_market_client: new FormControl('', [
      Validators.required
    ]),
    wallet: new FormControl('', [
      Validators.required,
      BlockchainValidator.validAddress(this.crypto)
    ]),
    method: new FormControl('', [
      Validators.required
    ])
  });

  formCardpay =  new FormGroup({
    orderXML: new FormControl(),
    sha512: new FormControl()
  });

  ngOnInit() {
    this.info.getPlatformStatus();
    this.getLevels();

    this.depositService.getDepositInfo()
      .then(data => {
        this.fiatRates = data;

        this.calcOutcome();
      });

    this.inp('client_currency').setValue(this.userService.user.currency);
  }

  ngOnDestroy() {
    clearTimeout(this.levelsInterval);
  }

  inp(element) {
    return this.form.get(element);
  }

  inpCardpay(element) {
    return this.formCardpay.get(element);
  }

  selectMarket(market) {
    this.selectedMarket = market;

    this.inp('market').setValue(market.pear);
    this.inp('wallet').setValue('');

    this.getLevels();
  }

  selectMethod(method) {
    if (method.active) {
      this.selectedPaymentMethod = method;

      this.inp('method').setValue(method.type);

      if (method.type === 'bank_transfer' && this.inp('original_amount').value === 100) {
        this.inp('original_amount').setValue(500);
      } else if (method.type === 'cardpay' && this.inp('original_amount').value === 500) {
        this.inp('original_amount').setValue(100);
      }

      this.calcOutcome();
    }
  }

  getLevels() {
    clearTimeout(this.levelsInterval);

    this.bitrexService.getLevels(this.selectedMarket.pear)
      .then(data => {
        this.currentCryptoRate = data;

        this.calcOutcome();

        this.levelsInterval = setTimeout(() => {
          this.getLevels();
        },  30000);
      })
      .catch(data => {
        console.log(data);
      });
  }

  setActiveInput(input) {
    this.activeInput = input;

    this.calcOutcome();
  }

  calcOutcome() {
    const currency = this.inp('client_currency').value.toUpperCase();
    let totalFee = 0;

    this.inp('rate_market_client').setValue(this.currentCryptoRate.ask);

    if (this.activeInput === 'fiat') {
      let amountUsd = this.amount = parseFloat(this.inp('original_amount').value || 0);

      for (const fee of this.selectedPaymentMethod.fees) {
        fee.amount = amountUsd * fee.value;
        totalFee += fee.amount;
      }

      amountUsd -= totalFee;

      if (currency !== 'USD' && this.fiatRates) {
        amountUsd = amountUsd * this.fiatRates[currency + 'USD'];
      }

      const crypto = (amountUsd / this.currentCryptoRate.ask).toFixed(8);

      this.inp('quantity_client').setValue(crypto);
    } else {
      // in USD
      let amountUsd = this.amount = parseFloat(this.inp('quantity_client').value || 0) * this.currentCryptoRate.ask;

      if (currency !== 'USD' && this.fiatRates) {
        amountUsd = amountUsd / this.fiatRates[currency + 'USD'];
      }

      for (const fee of this.selectedPaymentMethod.fees) {
        fee.amount = amountUsd * fee.value;
        totalFee += fee.amount;
      }

      this.amount = amountUsd + totalFee;

      this.inp('original_amount').setValue(this.amount.toFixed(2));
    }
  }

  submit() {
    if (this.form.invalid) {return; }

    // set user default currency
    if (!this.userService.user.isCurrencySet) {
      const override = {
        account_id: this.userService.user.account_id,
        currency: this.inp('client_currency').value
      };

      this.depositService.changeUserCurrency(this, override)
        .then(data => {
          this.userService.user.isCurrencySet = true;
          this.userService.user.currency = this.inp('client_currency').value;

          this.submitDeposit();
        });
    } else {
      this.submitDeposit();
    }
  }

  cardpaySubmit($event) {
    $event.target.submit();
  }

  submitDeposit() {
    // submit deposit
    // disabled inputs are not submited
    const overrideDeposit = {
      client_currency: this.inp('client_currency').value
    };

    this.depositService.deposit(this, overrideDeposit)
      .then(data => {
        if (this.inp('method').value === 'bank_transfer') {
          data.data.bank_account_info = {
            name: 'C3 TECH SOLUTIONS LIMITED',
            swift: 'CIURGB21',
            accountNum: 'GB80CIUR00997626995638',
            bank: 'CAURI LTD',
            country: 'United Kingdom'
          };
          const dialogRef = this.dialog.open(DepositSuccessBankwireComponent, {
            data: data.data
          });

          dialogRef.afterClosed().subscribe(result => {
            this.goToOrders();
          });
        } else {
          this.inAction = true;
          this.tx_key = data.data.tx_key;
          (<HTMLInputElement>document.getElementById('orderXML')).value = data.data.orderXML;
          (<HTMLInputElement>document.getElementById('sha512')).value = data.data.sha512;
          document.getElementById('checkoutSubmit').click();
        }
      });
  }

  goToOrders() {
    const translatedPath: any = this.localize.translateRoute('/platform/orders');
    this.router.navigate([translatedPath]);
  }

  get submitButton() {
    return this.inp('method').value === 'cardpay' ? 'button-buy' : 'button-complete-order';
  }

  get wp_url() {
    return this.utils.wp_url;
  }

  get isPlatformActive() {
    return this.info.isActive;
  }

  get cardPayUrl() {
    return this.utils.getOutgoingLinks.cardPayUrl;
  }
}
