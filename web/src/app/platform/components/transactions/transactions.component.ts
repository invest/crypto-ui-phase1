import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../services/transactions.service';
import { UtilsService } from '../../../shared/services/utils.service';
import { MatDialog } from '@angular/material';
import { DepositSuccessBankwireComponent } from '../deposit-success-bankwire/deposit-success-bankwire.component';
import { UserService } from '../../../user/services/user.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  public transactionsList;
  private allTransactions = [];
  private lastOpenOrder;
  public currentPage = 1;
  private maxServerPage = 0;
  private allServerResults = false;
  public range;
  public printTable: string;

  constructor(
    private transactions: TransactionsService,
    private utils: UtilsService,
    private dialog: MatDialog,
    private userService: UserService
  ) {
    this.range = utils.range;
  }

  ngOnInit() {
    this.getServerPages(1)
      .then(data => {
        this.goToPage(1);
      });
  }

  goToPage(page) {
    if (page === 'next') {
      page = this.currentPage + 1;
      if (this.allServerResults && page > this.pageCount) {
        page = this.pageCount;
      }
    } else if (page === 'prev') {
      page = this.currentPage - 1;

      if (page < 1) {
        page = 0;
      }
    }

    this.transactionsList = this.transactions.splitOrders(this.allTransactions, page);
    this.currentPage = page;

    if (page === this.pageCount && !this.allServerResults) {
      this.getServerPages(page);
    }
  }

  getServerPages(page) {
    const serverPage = this.maxServerPage + 1;

    return this.transactions.getCryptoReport(serverPage)
      .then(data => {
        this.allTransactions = this.allTransactions.concat(data.data);

        if (data.data.length < this.transactions.resultLimit) {
          this.allServerResults = true;
        }

        this.maxServerPage = serverPage;

        this.goToPage(page);

        return data.data;
      });
  }

  get pageCount() {
    return Math.ceil(this.allTransactions.length / this.transactions.resultsPerPage);
  }

  openPrinter(table) {
    this.printTable = table;
    setTimeout(() => {
      window.print();
    }, 100);
  }

  get printTableClass() {
    return this.printTable;
  }

  export(id) {
    this.utils.exportCSV(id);
  }

  toggleAccordion(order) {
    if (this.lastOpenOrder && this.lastOpenOrder.transaction_id !== order.transaction_id) {
      this.lastOpenOrder.detailsOpened = false;
    }

    order.detailsOpened = !order.detailsOpened;

    this.lastOpenOrder = order;
  }

  showBankWirePopup(order) {
    this.dialog.open(DepositSuccessBankwireComponent, {
      data: {
        popupType: 'info',
        transaction_id: order.transaction_id,
        amount: order.total,
        currency: order.currency,
        account_id: this.userService.user.account_id,
        bank_account_info: {
          name: 'C3 TECH SOLUTIONS LIMITED',
          swift: 'CIURGB21',
          accountNum: 'GB80CIUR00997626995638',
          bank: 'CAURI LTD',
          country: 'United Kingdom'
        }
      }
    });
  }
}
