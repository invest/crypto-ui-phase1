import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-deposit-success-bankwire',
  templateUrl: './deposit-success-bankwire.component.html',
  styleUrls: ['./deposit-success-bankwire.component.scss']
})
export class DepositSuccessBankwireComponent {

  public supportEmail: string;

  constructor(
    public dialogRef: MatDialogRef<DepositSuccessBankwireComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public utils: UtilsService
  ) {
    this.supportEmail = this.utils.supportEmail;
  }

  close(): void {
    this.dialogRef.close();
  }

}
