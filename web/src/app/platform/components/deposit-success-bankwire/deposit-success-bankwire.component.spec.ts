import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositSuccessBankwireComponent } from './deposit-success-bankwire.component';

describe('DepositSuccessBankwireComponent', () => {
  let component: DepositSuccessBankwireComponent;
  let fixture: ComponentFixture<DepositSuccessBankwireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositSuccessBankwireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositSuccessBankwireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
