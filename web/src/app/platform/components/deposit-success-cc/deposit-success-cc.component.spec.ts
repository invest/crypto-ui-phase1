import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositSuccessCcComponent } from './deposit-success-cc.component';

describe('DepositSuccessCcComponent', () => {
  let component: DepositSuccessCcComponent;
  let fixture: ComponentFixture<DepositSuccessCcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositSuccessCcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositSuccessCcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
