import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-deposit-success-cc',
  templateUrl: './deposit-success-cc.component.html',
  styleUrls: ['./deposit-success-cc.component.scss']
})
export class DepositSuccessCcComponent {

  constructor(
    public dialogRef: MatDialogRef<DepositSuccessCcComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) { }

  close(): void {
    this.dialogRef.close();
  }
}
