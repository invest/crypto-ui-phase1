import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Deposit3dComponent } from './deposit-3d.component';

describe('Deposit3dComponent', () => {
  let component: Deposit3dComponent;
  let fixture: ComponentFixture<Deposit3dComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Deposit3dComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Deposit3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
