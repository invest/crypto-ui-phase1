import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-deposit-3d',
  templateUrl: './deposit-3d.component.html',
  styleUrls: ['./deposit-3d.component.scss']
})
export class Deposit3dComponent {
  public iframeInfo = {};

  constructor(
    public dialogRef: MatDialogRef<Deposit3dComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) {
    this.iframeInfo = {
      url: data.redirect_url,
      checkFor: 'status'
    };
  }

  close(): void {
    this.dialogRef.close();
  }

  finishIframe(data) {
    this.dialogRef.close(data);
  }

}
