import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from './../shared/shared.module';
import { FooterLogoutComponent } from './components/footer-logout/footer-logout.component';
import { HeaderLogoutComponent } from './components/header-logout/header-logout.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SupportComponent } from './components/support/support.component';
import { ContactUsService } from './services/contact-us.service';

const routes = [
  { path: 'page-not-found', component: PageNotFoundComponent }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({}),
    LocalizeRouterModule.forChild(routes),
    ReactiveFormsModule,

    SharedModule
  ],
  providers: [
    ContactUsService
  ],
  declarations: [
    HeaderLogoutComponent,
    PageNotFoundComponent,
    FooterLogoutComponent,
    SupportComponent
  ],
  exports: [
    HeaderLogoutComponent,
    FooterLogoutComponent,
    SupportComponent
  ]
})
export class StaticModule { }
