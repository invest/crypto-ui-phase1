import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../../user/services/user.service';
import { ContactUsService } from '../../services/contact-us.service';
import { InfoService } from '../../../shared/services/info.service';
import { AppSettings } from '../../../shared/app-settings';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {

  public inAction = false;

  public isFormComplated = false;
  public countries;
  public ipCountry;
  public supportEmail: string;

  constructor(
    private userService: UserService,
    private contactUs: ContactUsService,
    private info: InfoService,
    private appSettings: AppSettings,
    private utils: UtilsService
  ) {
    this.supportEmail = this.utils.supportEmail;
  }

  form = new FormGroup({
    name: new FormControl(this.userService.user.displayName, [
      Validators.required
    ]),
    email: new FormControl(this.userService.user.email, [
      Validators.required,
      Validators.email
    ]),
    phone: new FormControl(this.userService.user.mobile_number, [
      Validators.required,
      Validators.minLength(7)
    ]),
    subject: new FormControl('', [
      Validators.required
    ]),
    message: new FormControl('', [
      Validators.required
    ]),
    country: new FormControl('', [
      Validators.required
    ])
  });

  ngOnInit() {
    this.info.getInfo()
      .then(data => {
        this.countries = data.countries_full;
        this.ipCountry = data.ip2country;

        this.inp('country').setValue(this.countries[this.ipCountry].telcode);
      });
  }

  inp(element) {
    return this.form.get(element);
  }

  submit() {
    // check if form valid
    if (this.form.invalid) {return; }

    const phone = {
      phone: '+' + this.inp('country').value + ' ' + this.inp('phone').value
    };
    // submit the form
    this.contactUs.sendContactUs(this, phone)
      .then(data => {
        this.isFormComplated = true;
      });
    }

    get wp_url() {
      return this.utils.wp_url;
    }
}
