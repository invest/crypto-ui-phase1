import { Component } from '@angular/core';

import { AppSettings } from '../../../shared/app-settings';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'footer-logout',
  templateUrl: './footer-logout.component.html',
  styleUrls: ['./footer-logout.component.scss']
})
export class FooterLogoutComponent {

  constructor(
    private appSettings: AppSettings,
    private utils: UtilsService
  ) { }

  get isLogged() {
    return this.appSettings.isLogged;
  }

  get wp_url() {
    return this.utils.wp_url;
  }

  get blog_url() {
    return this.utils.getOutgoingLinks.blog;
  }
}
