import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterLogoutComponent } from './footer-logout.component';

describe('FooterLogoutComponent', () => {
  let component: FooterLogoutComponent;
  let fixture: ComponentFixture<FooterLogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterLogoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterLogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
