import { animate, Component, keyframes, state, style, transition, trigger, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LocalizeRouterService } from 'localize-router';

import { AppSettings } from '../../../shared/app-settings';
import { UserService } from '../../../user/services/user.service';
import { SlideInOutAnimation } from '../../../shared/animations/slide-in-out.animation';
import { TransactionsService } from '../../../platform/services/transactions.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'header-logout',
  templateUrl: './header-logout.component.html',
  styleUrls: ['./header-logout.component.scss'],
  animations: [SlideInOutAnimation]
})
export class HeaderLogoutComponent {

  @ViewChild('menuRef') menuRef;

  public menuState = 'out';
  public localeList = [];

  constructor(
    private userService: UserService,
    private localize: LocalizeRouterService,
    private router: Router,
    private appSettings: AppSettings,
    private transactions: TransactionsService,
    private utils: UtilsService
  ) {
    this.localeList = appSettings.locales;
  }

  logout() {
    this.userService.logout();
  }

  get isLogged() {
    return this.appSettings.isLogged;
  }

  get user() {
    return this.userService.user;
  }

  get locale() {
    return this.appSettings.locale;
  }

  get openOrdersCount() {
    return this.transactions.openOrdersCount;
  }

  get wp_url() {
    return this.utils.wp_url;
  }

  changeLang(locale: string) {
    this.userService.changeLanguage(locale);
  }

  toggleMenu(force?) {
    if (force) {
      this.menuState = force;
    } else {
      this.menuState = (this.menuState === 'in') ? 'out' : 'in';
    }
  }

  @HostListener('document:click', ['$event']) clickOut($e: Event) {
    if (!this.menuRef.nativeElement.contains($e.target)) {
      this.toggleMenu('out');
    }
  }
}
