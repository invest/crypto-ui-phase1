import { Injectable } from '@angular/core';

import { HttpService } from '../../shared/services/http.service';

@Injectable()
export class ContactUsService {

  constructor(
    private http: HttpService,
  ) { }

  sendContactUs(thisRef, override?) {
    return this.http
      .post('contact_us', thisRef, null, override);
  }

}
