var env = 'local';

if (env == 'local') {
	var inv_api_env = 'https://plt1.coinshop.biz/api/v3/';
	var inv_api_platform_url = 'https://app1.coinshop.biz/';
	var inv_api_api_url = '';
	var inv_api_java_url = 'https://pltj1.corpidc.com/ticker/api/';
} else {
	var inv_api_env = 'https://##php_domain##/api/v3/';
	var inv_api_platform_url = 'https://##ui_domain##/';
	var inv_api_api_url = 'https://##ui_domain##/api/';
	var inv_api_java_url = 'https://##java_domain##/ticker/api/';
}
var reCapcha = '6LfH-lYUAAAAAFM2goSO9yF-L1KVSQKAGm8J_1kA';
var paramResetPasswordToken = "token";
var paramEmailConfirmed = "emailConfirmed";
var paramShowLogin = "showLogin";
var textParams = {
	linkAgreement: '{lang}legal-terms/',
	linkPrivacy: '{lang}legal-terms/?subp=2',
	supportEmail: 'support@coinshop.biz'
}

function INV_API_INIT() {
	var _this = this;
	this.apiSettings = {};
	this.translations = null;
	this.htmls = {};

	this.init = _init;
	this.login = function(params, callback) {return new _login(this, params, callback);};
	this.forgotPassword = function(params, callback) {return new _forgotPassword(this, params, callback);}
	this.confirmReset = function(params, callback) {return new _confirmReset(this, params, callback);}
	this.resetPassword = function(params, callback) {return new _resetPassword(this, params, callback);}
	this.register = function(params, callback) {return new _register(this, params, callback);}
	this.confirmEmail = function(params, callback) {return new _confirmEmail(this, params, callback);}
	this.resendConfirmEmail = function(params, callback) {return new _resendConfirmEmail(this, params, callback);}
	this.tradeBox = function(params, callback) {return new _tradeBox(this, params, callback);}

	function _init(params) {
		this.apiSettings = params;

		getStyles();

		getTranslations(this.apiSettings.lang)
			.then(function(data) {
				_this.translations = data;
			});
	}

	function _login(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		// find buttons and attach events (to open the popup)
		if (!this.settings.selector) {
			setTimeout(function() {displayLoginPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayLoginPopup.bind(this));
		}
	}

	function _forgotPassword(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		if (!this.settings.selector) {
			setTimeout(function() {displayForgotPasswordPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayForgotPasswordPopup.bind(this));
		}
	}

	function _confirmReset(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		if (!this.settings.selector) {
			setTimeout(function() {displayConfirmResetPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayConfirmResetPopup.bind(this));
		}
	}

	function _resetPassword(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		if (!this.settings.selector) {
			setTimeout(function() {displayResetPasswordPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayResetPasswordPopup.bind(this));
		}
	}

	function _register(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		// find buttons and attach events (to open the popup)
		if (!this.settings.selector) {
			setTimeout(function() {displayRegisterPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayRegisterPopup.bind(this));
		}
	}

	function _confirmEmail(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		if (!this.settings.selector) {
			setTimeout(function() {displayConfirmEmailPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayConfirmEmailPopup.bind(this));
		}
	}

	function _resendConfirmEmail(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		if (!this.settings.selector) {
			setTimeout(function() {displayResendEmailPopup.bind(_this)();}, 500);
		} else {
			this.elements.button = $(this.settings.selector);
	
			// show popup background
			this.elements.button.on('click', displayResendEmailPopup.bind(this));
		}
	}

	function _tradeBox(apiScope, params, callback) {
		var _this = this;
		this.apiScope = apiScope;
		this.settings = params;
		this.settings.callback = callback;
		this.elements = {};

		setTimeout(function() {displayTradeBox.bind(_this)();}, 500);
	}

	function displayLoginPopup() {
		var _this = this;

		_this.confirmedClass = (getUrlValue(paramEmailConfirmed) == 'true') ? ' inv-api-confirmed' : '';
		_this.afterResetPasswordClass = (_this.settings.afterResetPassword) ? ' inv-api-after-reset' : '';

		if (_this.afterResetPasswordClass != '') {
			_this.confirmedClass = '';
		}

		// add form
		createFormContainer.bind(this)('login', 'inv-api-mobile' + _this.confirmedClass + _this.afterResetPasswordClass)
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// atach event to form submit
						_this.elements.form.find('._invApiForm').submit(submitLogin.bind(_this));

						// get password input
						_this.elements.password = _this.elements.form.find('._invApiPassword');
						
						// add event to change password input to text
						_this.elements.form.find('._invApiShowPasswrod').on('click', showPassword.bind(_this));

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {closePopup.bind(_this)({
							responseCode: 1
						})});

						// login link action
						_this.elements.form.find('._invApiForgotLink').on('click', function() {
							INV_API.forgotPassword({
								type: 'popup'
							}, _this.settings.callback);

							closePopup.bind(_this)({
								responseCode: 1
							});
						});

						// register link action
						_this.elements.form.find('._invApiRegisterLink').on('click', function() {
							INV_API.register({
								type: 'popup'
							}, _this.settings.callback);

							closePopup.bind(_this)({
								responseCode: 1
							});
						});

						// add reCapcha
						_this.recaptchaIndex = grecaptcha.render('_invApiRecaptcha', {
							'sitekey' : reCapcha
						});
					});
			})
	}

	function displayForgotPasswordPopup() {
		var _this = this;

		// add form
		createFormContainer.bind(this)('forgot-password', '')
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// atach event to form submit
						_this.elements.form.find('._invApiForm').submit(submitForgorPassword.bind(_this));

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {
							INV_API.login({
								type: 'popup'
							}, _this.settings.callback);
	
							closePopup.bind(_this)({
								responseCode: 1
							});
						});
					});
			})
	}

	function displayConfirmResetPopup() {
		var _this = this;

		// add form
		createFormContainer.bind(this)('confirm-reset', '')
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						// translate
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {
							INV_API.login({
								type: 'popup'
							}, _this.settings.callback);
	
							closePopup.bind(_this)({
								responseCode: 1
							});
						});

						// request again
						_this.elements.form.find('._invApiRequestAgain').on('click', function() {
							INV_API.forgotPassword({
								type: 'popup'
							}, _this.settings.callback);

							closePopup.bind(_this)({
								responseCode: 1
							});
						});
					});
			})
	}

	function displayResetPasswordPopup() {
		var _this = this;

		// add form
		createFormContainer.bind(this)('reset-password', '')
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// atach event to form submit
						_this.elements.form.find('._invApiForm').submit(submitResetPassword.bind(_this));

						// onkeyup check password strnght
						// _this.elements.form.find('._invApiPassword').on('keyup', passwordStrength.bind(_this));

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// get password input
						_this.elements.password = _this.elements.form.find('._invApiPassword');
						_this.elements.passwordConfirm = _this.elements.form.find('._invApiPasswordConfirm');
						
						// add event to change password input to text
						_this.elements.form.find('._invApiShowPasswrod').on('click', showPassword.bind(_this));
						_this.elements.form.find('._invApiShowPasswrodConfirm').on('click', showPasswordConfirm.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {
							INV_API.login({
								type: 'popup'
							}, _this.settings.callback);
	
							closePopup.bind(_this)({
								responseCode: 1
							});
						});
					});
			})
	}

	function displayRegisterPopup() {
		var _this = this;

		// add form
		createFormContainer.bind(this)('register', 'inv-api-mobile')
			.then(function(data) {
				_this.elements.form = data;

				var presetEmail = (_this.elements.button) ? _this.elements.button.closest('form').find('input').val() : null;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// atach event to form submit
						_this.elements.form.find('._invApiForm').submit(submitRegister.bind(_this));

						// get email input
						_this.elements.email = _this.elements.form.find('._invApiEmail');
						if (presetEmail) {
							_this.elements.email.val(presetEmail);
						}

						// onkeyup check password strnght
						// _this.elements.form.find('._invApiPassword').on('keyup', passwordStrength.bind(_this));

						// get password input
						_this.elements.password = _this.elements.form.find('._invApiPassword');
						_this.elements.passwordConfirm = _this.elements.form.find('._invApiPasswordConfirm');
						
						// add event to change password input to text
						_this.elements.form.find('._invApiShowPasswrod').on('click', showPassword.bind(_this));
						_this.elements.form.find('._invApiShowPasswrodConfirm').on('click', showPasswordConfirm.bind(_this));

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {closePopup.bind(_this)({
							responseCode: 1
						})});

						// login link action
						_this.elements.form.find('._invApiLoginLink').on('click', function() {
							INV_API.login({
								type: 'popup'
							}, _this.settings.callback);

							closePopup.bind(_this)({
								responseCode: 1
							});
						});

						// add reCapcha
						_this.recaptchaIndex = grecaptcha.render('_invApiRecaptcha', {
							'sitekey' : reCapcha
						});
					});
			})
	}

	function displayConfirmEmailPopup() {
		var _this = this;

		// add form
		createFormContainer.bind(this)('confirm-email', '')
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						_this.apiScope.translations['registered-email'] = _this.apiScope.registeredEmail

						// translate
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {closePopup.bind(_this)({
							responseCode: 1
						})});

						_this.elements.form.find('._invApiResendEmail').on('click', function() {
							INV_API.resendConfirmEmail({
								type: 'popup'
							}, _this.settings.callback);
	
							closePopup.bind(_this)({
								responseCode: 1
							});
						});
					});
			})
	}
	
	function displayResendEmailPopup() {
		var _this = this;

		// add form
		createFormContainer.bind(this)('resend-email', '')
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						// translate
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						// fadein form
						_this.elements.form.fadeIn();

						// add escape lister to dom
						$(document).on('keyup', closePopupByEsc.bind(_this));

						// close popup by X
						_this.elements.form.find('._invApiCLose').on('click', function() {closePopup.bind(_this)({
							responseCode: 1
						})});

						submitResendConfirm.bind(_this)();
					});
			})
	}

	function displayTradeBox() {
		var _this = this;

		// add form
		getFormContainer.bind(this)('trade-box', '')
			.then(function(data) {
				_this.elements.form = data;

				// add translations
				isTranslationsReady(_this.apiScope.translations)
					.then(function() {
						// translate
						_this.elements.form.html(translateHtml.bind(_this)(_this.elements.form.html()));

						if (_this.settings.noButton) {
							_this.elements.form.find('._invApiRegisterLinkHolder').hide();
						} else {
							// register link action
							_this.elements.form.find('._invApiRegisterLink').on('click', function() {
								INV_API.register({
									type: 'popup'
								}, _this.settings.callback);
							});
						}

						_this.elements.currency = _this.elements.form.find('._invApiCurrency');
						_this.elements.market = _this.elements.form.find('._invApiMarket');
						_this.elements.amount = _this.elements.form.find('._invApiAmount');
						_this.elements.quantity = _this.elements.form.find('._invApiQuantity');

						_this.elements.currency.on('change', function() {
							calcOutcome.bind(_this)();
						});
						_this.elements.market.on('change', function() {
							getLevels.bind(_this)();
						});
						_this.elements.amount.on('keyup', function(e) {
							_this.activeInput = 'fiat';
							e.target.value = formatAmount(e.target.value);
							calcOutcome.bind(_this)();
						});
						_this.elements.quantity.on('keyup', function(e) {
							_this.activeInput = 'crypto';
							e.target.value = formatAmount(e.target.value);
							calcOutcome.bind(_this)();
						});

						_this.activeInput = 'fiat';
						_this.fiatRates = [];

						getDepositInfo()
							.then(function(data) {
								_this.fee = data.fee / 100;

								for (var i = 0; i < data.rates.length; i++) {
									_this.fiatRates[data.rates[i].currency_pair] = data.rates[i].value;
								}

								getLevels.bind(_this)();

								setInterval(function() {
									getLevels.bind(_this)();
								}, 30000);
							});
					});
			})
	}

	function getDepositInfo() {
		return $.ajax(
			inv_api_env + 'crypto-trade/rates',
			{
				method: 'GET',
				contentType: "application/json",
				headers: {
					"Authorization": "Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==",
					"Accept" : "application/json",
				}
			})
			.then(function(data) {
				data = JSON.parse(data);

				return {
					rates: data.data.rates,
					fee: data.data.fees.bank_transfer
				}
			})
			.catch(function(data) {
				console.error('Network or server error');
			});
	}

	function getLevelsAjax(market) {
		return $.ajax(
			inv_api_java_url + 'ticker1?market=' + market,
			{
				method: 'GET',
				contentType: "application/json",
				headers: {
					"Accept" : "application/json",
				}
			})
			.catch(function(data) {
				console.error('Network or server error');
			});
	}

	function getLevels() {
		var _this = this;

		getLevelsAjax(_this.elements.market.val())
			.then(function(data) {
				_this.currentCryptoRate = data;
				calcOutcome.bind(_this)();
			})
	}

	function calcOutcome() {
		var currency = this.elements.currency.val();
		var totalFee = 0;
	
		if (this.activeInput === 'fiat') {
			var amountUsd = parseFloat(this.elements.amount.val() || 0);
	
			totalFee = amountUsd * this.fee;
	
			amountUsd -= totalFee;
	
			if (currency !== 'USD' && this.fiatRates) {
				amountUsd = amountUsd * this.fiatRates[currency + 'USD'];
			}
	
		  	var crypto = (amountUsd / this.currentCryptoRate.ask).toFixed(8);
	
		  	this.elements.quantity.val(crypto);
		} else {
		  	// in USD
			var amountUsd = parseFloat(this.elements.quantity.val() || 0) * this.currentCryptoRate.ask;
		
			if (currency !== 'USD' && this.fiatRates) {
				amountUsd = amountUsd / this.fiatRates[currency + 'USD'];
			}
		
			totalFee = amountUsd * this.fee;
		
			var amount = amountUsd + totalFee;
		
			this.elements.amount.val(amount.toFixed(2));
		}
	}

	function createFormContainer(type, className) {
		var _this = this;
		return new Promise(function(resolve, reject) {
			// create container for login form
			var form = $('<div class="inv-api-popup-bgr ' + className + '"></div>');

			// attach event to close it on click
			form.on('click', function($e) {
				closePopup.bind(_this)({
					responseCode: 1,
					event: $e
				})
			});
			
			// atach to the body
			$('body').append(form);

			// add content
			if (_this.apiScope.htmls[type]) {
				form.html(_this.apiScope.htmls[type]);

				resolve(form);
			} else {
				getHtmlContent(type)
					.then(function(data) {
						_this.apiScope.htmls[type] = data;

						form.html(data);

						resolve(form);
					})
			}
		})
	}

	function getFormContainer(type, className) {
		var _this = this;
		return new Promise(function(resolve, reject) {
			// add content
			var form = $(_this.settings.selector);

			if (_this.apiScope.htmls[type]) {
				form.html(_this.apiScope.htmls[type]);

				resolve(form);
			} else {
				getHtmlContent(type)
					.then(function(data) {
						_this.apiScope.htmls[type] = data;

						form.html(data);

						resolve(form);
					})
			}
		})
	}

	function passwordStrength(e) {
		this.elements.passwordStrength = this.elements.form.find('._invApiPasswrodStrength');
		this.elements.passwordStrengthDspl = this.elements.form.find('._invApiPasswordStrengthDspl');

		if (this.lastPasswordStrength) {
			this.elements.passwordStrength.removeClass('inv-api-password-strength-' + this.lastPasswordStrength);
		}

		var pssStrength = inv_api_calc_password_strength($(e.target).val());
		this.lastPasswordStrength = pssStrength.dspl;

		this.elements.passwordStrength.addClass('inv-api-password-strength-' + this.lastPasswordStrength);
		// TODO translate
		this.elements.passwordStrengthDspl.html(pssStrength.text);
	}

	function showPassword() {
		var type = this.elements.password.attr('type');

		if (type === 'password') {
			this.elements.password.attr('type', 'text');
		} else {
			this.elements.password.attr('type', 'password');
		}
	}

	// TODO very very very ugly
	function showPasswordConfirm() {
		var type = this.elements.passwordConfirm.attr('type');

		if (type === 'password') {
			this.elements.passwordConfirm.attr('type', 'text');
		} else {
			this.elements.passwordConfirm.attr('type', 'password');
		}
	}

	function submitLogin(e) {
		// event.preventDefault();

		var _this = this;

		var reCaptchaResp = grecaptcha.getResponse(_this.recaptchaIndex);
		// if (env != 'prod') {
		// 	reCaptchaResp = 'fake one';
		// }

		this.elements.email = this.elements.form.find('._invApiEmail');
		this.elements.password = this.elements.form.find('._invApiPassword');
		this.elements.errors = this.elements.form.find('._invApiErrors');
		this.elements.submit = this.elements.form.find('._invApiSubmit');

		// reset errors
		this.elements.errors.hide();

		// basic validation
		if (!inv_api_isEmail(this.elements.email.val())) {
			showBasicError.bind(this)('error-MX_CHECK_FAILED_ERROR');
			return false;
		}
		if (this.elements.password.val().length < 7) {
			showBasicError.bind(this)('error-password-strength-general');
			return false;
		}
		if (reCaptchaResp != '') {
			// add loading class and disable
			this.elements.submit.addClass('inv-api-btn-loading');
			this.elements.submit.attr("disabled", true);

			// on submit make call to php api
			$.ajax(
				inv_api_env + 'login',
				{
					method: 'POST',
					contentType: "application/x-www-form-urlencoded",
					headers: {
						"Authorization": "Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==",
						"Accept" : "application/json",
					},
					data: JSON.stringify({
						'username': this.elements.email.val(),
						'password':this.elements.password.val(),
						'g-recaptcha-response':reCaptchaResp
					}),
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true
				})
				.then(function(data) {
					if (JSON.parse(data).data.email_confirmed == 0) {
						INV_API.confirmEmail({}, _this.settings.callback);

						closePopup.bind(_this)({
							responseCode: 1
						});
					} else {
						closePopup.bind(_this)({
							responseCode: 0
						});
					}
				})
				.catch(function(data) {
          grecaptcha.reset();

					if (data.responseText) {
						handleError.bind(_this)(JSON.parse(data.responseText).error, data.status);
					} else {
						handleError.bind(_this)();
					}
				})
				.always(function() {
					// remove disable and loading
					_this.elements.submit.removeClass('inv-api-btn-loading');
					_this.elements.submit.attr("disabled", false);
				});
		} else {
			showBasicError.bind(this)('error-login-general');
		}
		return false;
	}

	function submitForgorPassword(e) {
		// event.preventDefault();

		var _this = this;

		this.elements.email = this.elements.form.find('._invApiEmail');
		this.elements.errors = this.elements.form.find('._invApiErrors');
		this.elements.submit = this.elements.form.find('._invApiSubmit');

		// reset errors
		this.elements.errors.hide();

		// basic validation
		if (inv_api_isEmail(this.elements.email.val())) {
			// add loading class and disable
			this.elements.submit.addClass('inv-api-btn-loading');
			this.elements.submit.attr("disabled", true);

			// on submit make call to php api
			$.ajax(
				inv_api_env + 'forgot',
				{
					method: 'POST',
					contentType: "application/x-www-form-urlencoded",
					headers: {
						"Authorization": "Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==",
						"Accept" : "application/json",
					},
					data: JSON.stringify({
						'email': this.elements.email.val()
					}),
				})
				.then(function(data) {
					// show info popup
					INV_API.confirmReset({
						type: 'popup'
					}, _this.settings.callback);

					closePopup.bind(_this)({
						responseCode: 1
					});
					// handleError.bind(_this)(data.data);
					// closePopup.bind(_this)({
					// 	responseCode: 0
					// });
				})
				.catch(function(data) {
					if (data.responseText) {
						handleError.bind(_this)(JSON.parse(data.responseText).error, data.status);
					} else {
						handleError.bind(_this)();
					}
				})
				.always(function() {
					// remove disable and loading
					_this.elements.submit.removeClass('inv-api-btn-loading');
					_this.elements.submit.attr("disabled", false);
				});
		} else {
			showBasicError.bind(this)('error-MX_CHECK_FAILED_ERROR');
		}
		return false;
	}

	function submitResetPassword(e) {
		// event.preventDefault();

		var _this = this;

		this.elements.password = this.elements.form.find('._invApiPassword');
		this.elements.passwordConfirm = this.elements.form.find('._invApiPasswordConfirm');
		this.elements.errors = this.elements.form.find('._invApiErrors');
		this.elements.submit = this.elements.form.find('._invApiSubmit');

		// reset errors
		this.elements.errors.hide();

		// basic validation
		if (inv_api_calc_password_strength(this.elements.password.val()).goodEnough) {
			if (this.elements.password.val() === this.elements.passwordConfirm.val()) {
				// add loading class and disable
				this.elements.submit.addClass('inv-api-btn-loading');
				this.elements.submit.attr("disabled", true);

				// on submit make call to php api
				$.ajax(
					inv_api_env + 'password-reset',
					{
						method: 'POST',
						contentType: "application/x-www-form-urlencoded",
						headers: {
							"Authorization": "Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==",
							"Accept" : "application/json",
						},
						data: {
							'token': getUrlValue(paramResetPasswordToken),
							'password': this.elements.password.val(),
							'password_confirmation': this.elements.passwordConfirm.val()
						},
					})
					.then(function(data) {
						// show info popup
						INV_API.login({
							type: 'popup',
							afterResetPassword: true
						}, _this.settings.callback);

						closePopup.bind(_this)({
							responseCode: 1
						});
						// handleError.bind(_this)(data.data);
						// closePopup.bind(_this)({
						// 	responseCode: 0
						// });
					})
					.catch(function(data) {
						if (data.responseText) {
							handleError.bind(_this)(JSON.parse(data.responseText).error, data.status);
						} else {
							handleError.bind(_this)();
						}
					})
					.always(function() {
						// remove disable and loading
						_this.elements.submit.removeClass('inv-api-btn-loading');
						_this.elements.submit.attr("disabled", false);
					});
			} else {
				showBasicError.bind(this)('error-passwords-not-match');
			}
		} else {
			showBasicError.bind(this)('error-password-strength-general');
		}
		return false;
	}

	function submitRegister(e) {
		// event.preventDefault();

		var _this = this;

		this.elements.errors = this.elements.form.find('._invApiErrors');
		this.elements.submit = this.elements.form.find('._invApiSubmit');
		this.elements.acceptTerms = this.elements.form.find('._invApiAcceptTerms');

		var reCaptchaResp = grecaptcha.getResponse(_this.recaptchaIndex);
		// if (env != 'prod') {
		// 	reCaptchaResp = 'fake one';
		// }

		// reset errors
		this.elements.errors.hide();

		// basic validation
		if (reCaptchaResp != '' && this.elements.acceptTerms.is(":checked")) {
			if (inv_api_isEmail(this.elements.email.val())) {
				if (this.elements.password.val() === this.elements.passwordConfirm.val()) {
					if (inv_api_calc_password_strength(this.elements.password.val()).goodEnough ) {
						// add loading class and disable
						this.elements.submit.addClass('inv-api-btn-loading');
						this.elements.submit.attr("disabled", true);

						// on submit make call to php api
						$.ajax(
							inv_api_env + 'registration',
							{
								method: 'POST',
								contentType: "application/json",
								headers: {
									"Authorization": "Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==",
									"Accept" : "application/json",
								},
								data: JSON.stringify({
									'email': this.elements.email.val(),
									'password': this.elements.password.val(),
									'password_confirmation': this.elements.passwordConfirm.val(),
									'g-recaptcha-response': reCaptchaResp,
                  'locale': this.apiScope.apiSettings.lang
								}),
								xhrFields: {
									withCredentials: true
								},
								crossDomain: true
							})
							.then(function() {
								_this.apiScope.registeredEmail = _this.elements.email.val();

								INV_API.confirmEmail({}, _this.settings.callback);

								closePopup.bind(_this)({
									responseCode: 1
								});
							})
							.catch(function(data) {
                grecaptcha.reset();
                
								if (data.responseText) {
									handleError.bind(_this)(JSON.parse(data.responseText).error, data.status);
								} else {
									handleError.bind(_this)();
								}
							})
							.always(function() {
								// remove disable and loading
								_this.elements.submit.removeClass('inv-api-btn-loading');
								_this.elements.submit.attr("disabled", false);
							});
						} else {
							showBasicError.bind(this)('error-password-strength-general');
						}
				} else {
					showBasicError.bind(this)('error-passwords-not-match');
				}
			} else {
				showBasicError.bind(this)('error-MX_CHECK_FAILED_ERROR');
			}
		} else {
			showBasicError.bind(this)('error-register-general');
		}
		
		return false;
	}

	function submitResendConfirm(e) {
		// event.preventDefault();

		var _this = this;

		this.elements.errors = this.elements.form.find('._invApiErrors');

		// reset errors
		this.elements.errors.hide();

		// on submit make call to php api
		$.ajax(
			inv_api_env + 'registration/resend-confirmation',
			{
				method: 'GET',
				contentType: "application/x-www-form-urlencoded",
				headers: {
					"Authorization": "Basic QjYxQTZENTQyRjkwMzY1NTBCQTlDNDAxQzgwRjAwRUY6RTMyRDQzNkU3QjgzNEUyODk5QUI0QTIxMTVDQzI3M0M3QTU5N0EwMzBGRDc0M0NDQjhCOUQzQ0JDRDhGMzgyMg==",
					"Accept" : "application/json",
				},
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true
			})
			.then(function(data) {

			})
			.catch(function(data) {
				if (data.responseText) {
					handleError.bind(_this)(JSON.parse(data.responseText).error, data.status);
				} else {
					handleError.bind(_this)();
				}
			});
			
		return false;
	}

	function closePopup(params) {
		// close only if clicked on bgr
		if (params.event && !$(params.event.target).hasClass('inv-api-popup-bgr')) {return; }

		this.elements.form.fadeOut(function() {this.remove();});

		/*responseCode {
			0: success
			1: popup closed
		}*/
		if (this.settings.callback) {
			this.settings.callback({
				responseCode: params.responseCode,
				popupSettings: this.settings
			});
		} else if(params.responseCode == 0) {
			if (this.apiScope.apiSettings.platformUrl) {
				window.location.href = this.apiScope.apiSettings.platformUrl;
			} else {
				window.location.href = inv_api_platform_url;
			}
		}
	}

	function closePopupByEsc(e) {
		if (e.keyCode === 27) {
			$(document).off('keyup');

			closePopup.bind(this)({
				responseCode: 1
			});
		}
	}

	function handleError(error, statusCode) {
		if (statusCode && statusCode == 422) {
			var errors = '';

			for (var el in error) {
				if (error[el].length > 0) {
					for (var i = 0; i < error[el].length; i++) {
						// if (errors != '') {
						// 	errors += ', ';
						// }
	
						errors = this.apiScope.translations['error-' + error[el][i]];
					}
				}
			}

			showBasicError.bind(this)(errors, true);
		} else if (typeof error == 'string') {
			showBasicError.bind(this)('<span class="inv-api-success">' + error + '</span>', true);
		} else if (error && error.message != '') {
			showBasicError.bind(this)(error.message, true);
		} else if (error) {
			showBasicError.bind(this)('error-' + error.code);
		} else {
			showBasicError.bind(this)('error-network');
		}
	}

	function showBasicError(key, skipTranslate) {
		if (skipTranslate) {
			this.elements.errors.html(key);
		} else {
			this.elements.errors.html(this.apiScope.translations[key]);
		}
		this.elements.errors.slideDown();
	}

	function getHtmlContent(src) {
		return new Promise(function(resolve, reject) {
			$.ajax(inv_api_api_url + src + '-form.html')
				.then(function(data) {
					resolve(data);
				})
				.catch(function() {
					console.error('Problem getting html for ' + src);
				});
		});
	}

	function getStyles() {
		$.ajax(inv_api_api_url + 'styles.css')
			.then(function(data) {
				$('head').append('<style type="text/css">' + data + '</style>');
			})
			.catch(function() {
				console.error('Problem getting style');
			});
	}

	function getTranslations(lang) {
		return new Promise(function(resolve, reject) {
			$.ajax(inv_api_api_url + 'assets/i18n/api-' + lang + '.json')
				.then(function(data) {
					resolve(data);
				})
				.catch(function() {
					console.error('Problem getting translations');
				});
		});
	}

	function isTranslationsReady(translations) {
		var timer;
		return new Promise(function(resolve) {
			timer = setInterval(function() {
				if (translations) {
					clearInterval(timer);
					resolve();
				}
			}, 100);
		});
	}

	// replace in html all {{key}} with coresponding translation from i18.json
	function translateHtml(html) {
		var _this = this;

		return html.replace(/\{{(.*?)\}}/g, function(match) {
			var key = match.replace('{{', '').replace('}}', '');

			if (_this.apiScope.translations[key]) {
				return _this.apiScope.translations[key].replace(/\{(.*?)\}/g, function(matchLinks) {
					var keyLinks = matchLinks.replace('{', '').replace('}', '');

					if (textParams[keyLinks].indexOf('{lang}') > -1) {
						return textParams[keyLinks].replace('{lang}', (_this.apiScope.apiSettings.lang == 'en') ? '' : _this.apiScope.apiSettings.lang + '/');
					} else {
						return textParams[keyLinks];
					}
				});
			} else {
				return '';
			}
		});
	}
}

function inv_api_isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function inv_api_calc_password_strength(password) {
	var strength = {
		goodEnough: false
	};

    var strongRegex = new RegExp('^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$', 'g');
    var mediumRegex = new RegExp('^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$', 'g');
    var enoughRegex = new RegExp('(?=.{6,}).*', 'g');

    if (false == enoughRegex.test(password)) {
		strength.dspl = 'tooshort';
		strength.text =  'Weak';
    } else if (strongRegex.test(password)) {
        strength.dspl =  'strong';
        strength.goodEnough =  true;
        strength.text =  'Strong';
    } else if (mediumRegex.test(password)) {
        strength.dspl =  'good';
		strength.goodEnough =  true;
		strength.text =  'Medium';
    } else {
		strength.dspl =  'tooweak'
		strength.text =  'Weak';
    }

    return strength;
}

function getUrlValue(key) {
	var params = window.location.search;
	var query = params.substring(1);
	var vars = query.split('&');

	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (key === pair[0]) {
		return decodeURIComponent(pair[1]);
		}
	}
	return null;
}

function formatAmount(amount) {
	var parts = amount.split('.');
	var output = parts[0].replace(/\D/g, '');

	if (parts.length > 1) {
		output += '.' + parts[1].replace(/\D/g, '');
	}

	return output;
}

var INV_API = new INV_API_INIT();

setTimeout(function() {
	if (getUrlValue(paramResetPasswordToken)) {
		INV_API.resetPassword({
			type: 'popup'
		});
	} else if (getUrlValue(paramEmailConfirmed) || getUrlValue(paramShowLogin)) {
		INV_API.login({
			type: 'popup'
		});
	}
}, 500);